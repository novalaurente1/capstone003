const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const YogaClassScheduleSchema = new Schema({
  yogaclassName: String,
  yogaclassId: String,
  instructorName: String,
  instructorId: String,
  date: String,
  startTime: String,  
  endTime: String
})

module.exports = mongoose.model("YogaClassSchedule", YogaClassScheduleSchema);