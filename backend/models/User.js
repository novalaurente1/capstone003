const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const moment = require('moment');

const UserSchema = new Schema({
  dateRegistered: {
		type: String,
		default: moment(new Date).format('LL')
  },
  customerCode: String,
	name: String,
	mobileNumber: String,
	email: {
		type: String,
		required: true,
		unique: true
	},
	password: {
    type: String,
		required: true
	},
  isAdmin: {
    type: Boolean,
    default: false
	},
	profilePicture: String
})

module.exports = mongoose.model('User', UserSchema);