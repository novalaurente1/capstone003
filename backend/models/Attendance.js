const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const AttendanceSchema = new Schema({
  userId: String,
  userCustomerCode: String,
  userName: String,
  classId: String,
  className: String,
  classDate: String,
  classStartTime: String,
  instructorId: String,
  instructorName: String,
  attendance: {
		type: String,
		default: "Booked"
	}
})

module.exports = mongoose.model("Attendance", AttendanceSchema);