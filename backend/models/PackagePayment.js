const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PackagePaymentSchema = new Schema({
  userId: String,
  userName: String,
  userCustomerCode: String,
  packageId: String,
  packageName: String,
  userSessions: Number,
  packageSessions: Number,
  paymentId: String,
  packagePrice: Number
})

module.exports = mongoose.model("PackagePayment", PackagePaymentSchema);