const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const moment = require('moment');

const YogaPackageSchema = new Schema({
  name: String,
  type: {
    type: String,
    default: "Limited"
  },
  numberOfSessions: Number,
  price: Number,
  validity: {
    type: String,
    default: moment().endOf("year").format('LL')
  },
  branchName: String, 
  branchId: String
});

module.exports = mongoose.model("YogaPackage", YogaPackageSchema);