const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const InstructorSchema = new Schema({
  name: String,
  image: String,
  description: String
})

module.exports = mongoose.model("Instructor", InstructorSchema);