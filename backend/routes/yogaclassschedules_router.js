const express = require('express');

const YogaClassScheduleRouter = express.Router();

const YogaClassScheduleModel = require('../models/YogaClassSchedule');

YogaClassScheduleRouter.post('/addschedule', async(req, res)=>{
  try {
    let schedule = await YogaClassScheduleModel({
      yogaclassName: req.body.yogaclassName,
      yogaclassId: req.body.yogaclassId,
      instructorName: req.body.instructorName,
      instructorId: req.body.instructorId,
      branchName: req.body.branchName,
      branchId: req.body.branchId,
      date: req.body.date,
      startTime: req.body.startTime,
      endTime: req.body.endTime
      }).save();

      res.send(schedule);

  } catch (error) {
    console.log(error);
  }
})

YogaClassScheduleRouter.get('/showschedules', async(req, res)=>{
  try {
    let schedules = await YogaClassScheduleModel.find();

    res.send(schedules);
  } catch (error) {
    console.log(error);
  }
})

YogaClassScheduleRouter.get('/countschedules', async(req, res)=>{
  try {
    const totalSchedules = await YogaClassScheduleModel.countDocuments();

    res.send({
      totalSchedules 
    });
  } catch (error) {
    console.log(error);
  }
})

YogaClassScheduleRouter.get('/showschedule/:id', async(req, res)=>{
  try {
    let schedule = await YogaClassScheduleModel.findById(req.params.id);

    res.send(schedule);
  } catch (error) {
    console.log(error);
  }
})

YogaClassScheduleRouter.put('/updateschedule/:id', async(req, res)=>{
  try {
    let schedule = await YogaClassScheduleModel.findById(req.params.id);

    if(!schedule){
      return res
      .status(404)
      .send(`Schedule can't be found`);
    }

    let condition = {_id:req.params.id};
    let updates = {
      yogaclassName: req.body.yogaclassName,
      yogaclassId: req.body.yogaclassId,
      instructorName: req.body.instructorName,
      instructorId: req.body.instructorId,
      branchName: req.body.branchName,
      branchId: req.body.branchId,
      startTime: req.body.startTime,
      endTime: req.body.endTime
    }

    let updatedSchedule = await YogaClassScheduleModel.findOneAndUpdate(condition, updates, {new:true});

    res.send(updatedSchedule);
  } catch (error) {
    console.log(error);
  }
})

YogaClassScheduleRouter.delete('/deleteschedule/:id', async(req, res)=>{
  try {
    let deletedSchedule = await YogaClassScheduleModel.findByIdAndDelete(req.params.id);

    res.send(deletedSchedule);
  } catch (error) {
    console.log(error); 
  }
})

module.exports = YogaClassScheduleRouter;