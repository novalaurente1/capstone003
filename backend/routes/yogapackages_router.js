const express = require('express');

const YogaPackageRouter = express.Router();

const YogaPackageModel = require("../models/YogaPackage");
const { Parser } = require('json2csv');
const fs = require('fs');

YogaPackageRouter.post('/addyogapackage', async(req,res)=>{
  try {
    let yogapackage = await YogaPackageModel({
      name: req.body.name,
      numberOfSessions: req.body.numberOfSessions,
      price: req.body.price,
      branchName: req.body.branchName,  
      branchId: req.body.branchId
    }).save();

    res.send(yogapackage);

  } catch (error) {
    console.log(error);
  }
});

YogaPackageRouter.get('/showyogapackages', async(req,res)=>{
  try {
    let yogapackages = await YogaPackageModel.find();
    res.send(yogapackages);
  } catch (error) {
    console.log(error);
  }
});

YogaPackageRouter.get('/countpackages', async(req,res)=>{
  try {
    let totalPackages = await YogaPackageModel.countDocuments();
    res.send({totalPackages});
  } catch (error) {
    console.log(error);
  }
});

YogaPackageRouter.get('/exportyogapackages', async(req,res)=>{
  try {
    let yogapackages = await YogaPackageModel.find();

    const parser = new Parser({
      fields: ['name', 'type', 'numberOfSessions', 'price']
    });

    const csv = parser.parse(yogapackages);

    const path = '/tmp/export.csv';

    fs.writeFile(path, csv, function(err,data) {
      if (err) {throw err;}
      else{ 
        res.download(path); // This is what you need
      }
    }); 
  } catch (error) {
    console.log(error);
  }
});

YogaPackageRouter.get('/showyogapackage/:id', async(req, res)=>{
  try {
    let yogapackage = await YogaPackageModel.findById(req.params.id);

    res.send(yogapackage);
  } catch (error) {
    console.log(error);
    
  }
});

YogaPackageRouter.put('/updateyogapackage/:id', async(req, res)=>{
  try {
    let yogapackage = await YogaPackageModel.findById(req.params.id);

    if(!yogapackage){
      return res
      .status(404)
      .send(`Package can't be found`);
    }

    let condition = {_id:req.params.id};
    let updates = {
      name: req.body.name,
      numberOfSessions: req.body.numberOfSessions,
      price: req.body.price,
      branch: req.body.branch, 
      branchId: req.body.branchId
    }

    let updatedYogaPackage = await YogaPackageModel.findOneAndUpdate(condition, updates, {new:true});

    res.send(updatedYogaPackage);

  } catch (error) {
    console.log(error); 
  }
});

YogaPackageRouter.delete('/deleteyogapackage/:id', async(req, res)=>{
  try {
    let deletedYogaPackage = await YogaPackageModel.findByIdAndDelete(req.params.id);

    res.send(deletedYogaPackage);
  } catch (error) {
    console.log(error); 
  }
})

module.exports = YogaPackageRouter;