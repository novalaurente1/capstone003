const express = require('express');

const InstructorRouter = express.Router();

const InstructorModel = require('../models/Instructor');

InstructorRouter.post('/addinstructor', async(req, res)=>{
  try {
    let instructor = await InstructorModel({
      name: req.body.name,
      image: req.body.image,
      description: req.body.description
    }).save();

    res.send(instructor);

  } catch (error) {
    console.log(error);
  }
});

InstructorRouter.get('/showinstructors', async(req, res)=>{
  try {
    let instructors = await InstructorModel.find();

    res.send(instructors);

  } catch (error) {
    console.log(error);
  }
})

InstructorRouter.get('/showinstructor/:id', async(req, res)=>{
  try {
    let instructor = await InstructorModel.findById(req.params.id);

    res.send(instructor);

  } catch (error) {
    console.log(error);
  }
})

InstructorRouter.put('/updateinstructor/:id', async(req, res)=>{
  try {
    let instructor = await InstructorModel.findById(req.params.id);

    if(!instructor){
      return res
      .status(404)
      .send(`Instructor can't be found`);
    }

    let condition = {_id:req.params.id};
    let updates = {
      name: req.body.name,
      image: req.body.image,
      description: req.body.description
    }

    let updatedInstructor = await InstructorModel.findByIdAndUpdate(condition, updates, {new:true});

    res.send(updatedInstructor);

  } catch (error) {
    console.log(error);
  }
})

InstructorRouter.delete('/deleteinstructor/:id', async(req, res)=>{
  try {
    let deletedInstructor = await InstructorModel.findByIdAndDelete(req.params.id);

    res.send(deletedInstructor);

  } catch (error) {
    console.log(error);
  }
})

module.exports = InstructorRouter;