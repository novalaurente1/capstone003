const express = require('express');
const UserRouter = express.Router();
const UserModel = require('../models/User');
const PackagePaymentModel = require('../models/PackagePayment');
const AttendanceModel = require('../models/Attendance');
const bcrypt = require('bcryptjs');
const auth = require('../middleware/auth_middleware');

UserRouter.post('/register', async(req, res)=>{
  if(!req.body.email) return res.status(400).send('Email is required');

	if(!req.body.password) return res.status(400).send('Password is required');

	let user = UserModel({
    name: req.body.name,
    mobileNumber: req.body.mobileNumber,
		email: req.body.email
	});

	let salt = bcrypt.genSaltSync(10);
	let hashed =bcrypt.hashSync(req.body.password, salt);
	user.password = hashed;

	try {
		user = await user.save();
		res.send(user)
	} catch(e) {
		res.status(400).send("Invalid Data");
		console.log(e);
	}
});

UserRouter.get('/admin/showusers', async(req,res)=>{
  try {
    let users = await UserModel.find({ isAdmin: false});
    res.send(users);
  } catch (error) {
    console.log(error);
  }
});

UserRouter.get('/admin/showuserbyid/:id', async(req,res)=>{
  try {
    let users = await UserModel.findById(req.params.id);
    res.send(users);
  } catch (error) {
    console.log(error);
	}
});

UserRouter.get('/admin/showclassesbyuserid/:id', async(req,res)=>{
  try {
    let userSchedules = await AttendanceModel.find({userId: req.params.id});
    res.send(userSchedules);
  } catch (error) {
    console.log(error);
	}
});

UserRouter.get('/admin/countusers', async(req,res)=>{
  try {
    const totalCustomers = await UserModel.countDocuments({ isAdmin: false});
    res.send({totalCustomers});
  } catch (error) {
    console.log(error);
	}
});

UserRouter.get('/userpackages/:id', async(req,res)=>{
  try {
    const packagePayments = await PackagePaymentModel.find({
			userId: req.params.id,
			packageSessions: {
				$gt: 0
			}
		});
		
		let packagePayment = null;
		
		if(packagePayments.length > 0) {
			packagePayment = packagePayments[0];
		}

    res.send({packagePayment});
  } catch (error) {
    console.log(error);
  }
});

UserRouter.patch('/updateadminstatus/:id', auth, async(req, res)=>{
	try {
		let condition = {_id:req.params.id}
		let update = {isAdmin: req.body.isAdmin}

		let updatedUser = await UserModel.findOneAndUpdate(condition, update, {new:true})

		res.send(updatedUser)
	} catch(e) {
		res.status(400).send('error')
	}
});

UserRouter.patch('/updateprofilepicture/:id', async(req, res)=>{
	try {
		let condition = {_id:req.params.id}
		let update = {profilePicture: req.body.profilePicture}

		let updatedUser = await UserModel.findOneAndUpdate(condition, update, {new:true})

		res.send(updatedUser)
	} catch(e) {
		res.status(400).send('error')
	}
});

UserRouter.delete('/deleteuser/:id', async(req, res)=>{
  try {
    let deletedUser = await UserModel.findByIdAndDelete(req.params.id);

    res.send(deletedUser);
  } catch (error) {
    console.log(error); 
  }
});

module.exports = UserRouter;