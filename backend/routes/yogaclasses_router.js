const express = require('express');

const YogaClassRouter = express.Router();

const YogaClassModel = require('../models/YogaClass');

YogaClassRouter.post('/addyogaclass', async(req, res)=>{
  try {
    let yogaclass = await YogaClassModel({
      name: req.body.name,
      description: req.body.description
    }).save();

    res.send(yogaclass);

  } catch (error) {
    console.log(error);
  }
})

YogaClassRouter.get('/showyogaclasses', async(req, res)=>{
  try {
    let yogaclasses = await YogaClassModel.find();

    res.send(yogaclasses);
  } catch (error) {
    console.log(error);
  }
})

YogaClassRouter.get('/showyogaclass/:id', async(req, res)=>{
  try {
    let yogaclass = await YogaClassModel.findById(req.params.id);

    res.send(yogaclass);
  } catch (error) {
    console.log(error);
  }
})

YogaClassRouter.put('/updateyogaclass/:id', async(req, res)=>{
  try {
    let yogaclass = await YogaClassModel.findById(req.params.id);

    if(!yogaclass){
      return res
      .status(404)
      .send(`Yogaclass can't be found`);
    }

    let condition = {_id:req.params.id};
    let updates = {
      name: req.body.name,
      description: req.body.description
    }

    let updatedYogaClasses = await YogaClassModel.findOneAndUpdate(condition, updates, {new:true})

    res.send(updatedYogaClasses);
  } catch (error) {
    console.log(error);
  }
})

YogaClassRouter.delete('/deleteyogaclass/:id', async(req, res)=>{
  try {
    let deletedYogaClass = await YogaClassModel.findByIdAndDelete(req.params.id);

    res.send(deletedYogaClass);
  } catch (error) {
    console.log(error);
  }
})

module.exports = YogaClassRouter;