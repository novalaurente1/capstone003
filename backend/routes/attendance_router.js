const express = require('express');

const AttendanceRouter = express.Router();

const AttendanceModel = require('../models/Attendance');
const PackagePaymentModel = require('../models/PackagePayment');

AttendanceRouter.post('/addstudent', async(req, res)=>{
  try {
    // validate first if the user already booked the class
    let totalAttendance = await AttendanceModel.countDocuments({
      userId: req.body.userId,
      classId: req.body.classId
    });

    if (totalAttendance > 0) {
      // if the user's attendance for the class exists, return an error.
      res.send({
        error: 'Booking already exists.'
      });
    } else {
      // if the user's attendance for the class does not exist, proceed with creating an attendance
      // create an attendance
      let attendance = await AttendanceModel({
        userId: req.body.userId,
        userCustomerCode: req.body.userCustomerCode,
        userName: req.body.userName,
        classId: req.body.classId,
        className: req.body.className,
        classDate: req.body.classDate,
        classStartTime: req.body.classStartTime,
        instructorId: req.body.instructorId,
        instructorName: req.body.instructorName
      }).save();

      res.send(attendance);
    }
  } catch (error) {
    console.log(error);
  }
})

AttendanceRouter.get('/showattendancebyschedule/:id', async(req,res)=>{
  try {
    let attendance = await AttendanceModel.find({classId:req.params.id});
    res.send(attendance);
  } catch (error) {
    console.log(error);
  }
});

AttendanceRouter.get('/countattendees/:id', async(req, res)=>{
  try {
    const totalAttendees = await AttendanceModel.countDocuments({classId:req.params.id});

    res.send({
      totalAttendees
    });
  } catch (error) {
    console.log(error);
  }
})

AttendanceRouter.patch('/updateattendance/:id', async(req,res)=>{
  try {
    const attendanceStatus = req.body.attendance;

		let condition = {_id:req.params.id}
		let update = {
			attendance: attendanceStatus
		}
    let updatedAttendance = await AttendanceModel.findOneAndUpdate(condition, update, {new:true})
    
    const packagePayment = await PackagePaymentModel.findOne({
      userId: updatedAttendance.userId,
      userSessions: {
        $gt: 0
      }
    });
    if (packagePayment !== null) {
      if (attendanceStatus === 'Present') {
        packagePayment.userSessions = packagePayment.userSessions - 1;
      } else {
        packagePayment.userSessions = packagePayment.userSessions + 1;
      }
      packagePayment.save();
    }
    
		res.send(updatedAttendance);
	} catch(e) {
		console.log(e);
	}
});

AttendanceRouter.delete('/deletestudent/:id', async(req, res)=>{
  try {
    let deletedStudent = await AttendanceModel.findByIdAndDelete(req.params.id);

    res.send(deletedStudent);
  } catch (error) {
    console.log(error); 
  }
})

module.exports = AttendanceRouter;