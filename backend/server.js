const express = require("express");
const app = express();
const mongoose = require("mongoose");
const config = require("./config");
const cors = require("cors");
const multer = require("multer");
const stripe = require('stripe')('sk_test_ke1xf7xWpTfj3h09uYi2hexP00Ni4QdN8P');

let storage = multer.diskStorage({
	destination: (req, file, cb) =>{
		cb(null, 'public/images/uploads')
	},
	filename: (req, file, cb) =>{
		cb(null, Date.now() + "-" + file.originalname)
	}
})

const upload = multer({storage});

const path = require('path');

app.use(express.static(path.join(__dirname, 'public')));

const PackagePaymentModel = require('./models/PackagePayment');
const YogaPackageModel = require('./models/YogaPackage');
const UserModel = require('./models/User');

const databaseUrl = process.env.DATABASE_URL || "mongodb+srv://admin:P@ssw0rd@cluster0-ewfxq.mongodb.net/ascendyoga?retryWrites=true&w=majority"

mongoose.connect(databaseUrl, {
	useNewUrlParser:true,
	useUnifiedTopology:true,
	useFindAndModify: false,
	useCreateIndex: true
}).then(()=>{
	console.log("Remote Database Connection Established")
});

app.use(express.urlencoded({extended:false}));

app.use(express.json());

app.use(cors());

app.listen(config.port, () => {
  console.log(`Listening on Port ${config.port}`);
});

// package api
const yogapackages = require("./routes/yogapackages_router");
app.use('/admin', yogapackages);

// instructor api
const instructors = require("./routes/instructors_router");
app.use('/admin', instructors);

// yogaclass api
const yogaclasses = require("./routes/yogaclasses_router");
app.use('/admin', yogaclasses);

// schedule api
const yogaclassschedules = require("./routes/yogaclassschedules_router");
app.use('/admin', yogaclassschedules);

const users = require("./routes/users_router");
app.use('/', users);

const auth = require("./routes/auth_router");
app.use('/', auth);

app.post('/upload', upload.single('image'), (req, res)=>{
	if(req.file){
		res.json({
			imageUrl: `images/uploads/${req.file.filename}`,
			filename: req.file.filename
		})
	}else{
		res.status("409").json("No files to upload")
	}
})

app.post('/charge', async (req, res)=> {
	console.log(req.body);
	try {
		const {tokenId, description, amount, packageId, userId} = req.body;

		// Charge the customer
		const chargeResponse = await stripe.charges.create({
			amount: amount * 100,
			currency: "php",
			source: tokenId,
			description: "Sample payment for ascend yoga - " + description
		});

		const chargeId = chargeResponse.id;

		const package = await YogaPackageModel.findById(packageId);
		const user = await UserModel.findById(userId);

		const packagePayment = await PackagePaymentModel({
			userId: user._id,
			userName: user.name,
			userCustomerCode: user.customerCode,
			packageId: package._id,
			packageName: package.name,
			packageName: package.name,
			packagePrice: package.price,
			userSessions: package.numberOfSessions,
			packageSessions: package.numberOfSessions,
			paymentId: chargeId
    }).save();

		res.send(packagePayment);
	} catch(e) {
		// statements
		console.log(e);
		res.send(e);
	}
})

app.get('/revenue', async(req, res)=>{
	const revenue = await PackagePaymentModel.aggregate([
		{ 
			$group: {
				_id: null, 
				total: {
					$sum: '$packagePrice'
				}
			}
		}
	]);

	res.send({
		revenue: revenue[0].total
	})
})

app.get('/admin/showpackagepayments', async(req,res)=>{
	try {
    let packagepayments = await PackagePaymentModel.find();
    res.send(packagepayments);
  } catch (error) {
    console.log(error);
  }
})

const attendance = require("./routes/attendance_router");
app.use('/', attendance);

