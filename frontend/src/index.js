import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
// import YogaPackages from './admin/pages/yogapackages/YogaPackages';
// import YogaClasses from './admin/pages/yogaclasses/YogaClasses';
// import DisplayYogaPackages from './user/pages/displayyogapackages/DisplayYogaPackages';
// import DisplayClassSchedules from './user/pages/displayclassschedules/DisplayClassSchedules';
// import Checkout from './user/pages/checkout/Checkout';
// import YogaClassSchedules from './admin/pages/yogaclassschedules/YogaClassSchedules';
// import UserHomepage from './user/layout/UserHomepage';
import * as serviceWorker from './serviceWorker';

// day-picker
import 'react-day-picker/lib/style.css';

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
