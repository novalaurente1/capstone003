import React, { useState, Fragment } from 'react';
import { FormInput } from '../components';
import { Button } from 'reactstrap';
import axios from 'axios';
import { Navibar } from './components';

const base_url = process.env.REACT_APP_API_BASE_URL;

const Register = () => {
  const [name, setName] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleNameChange = e => {
    setName(e.target.value);
    // console.log(e.target.value);
  };

  const handleMobileNumberChange = e => {
    setMobileNumber(e.target.value);
    // console.log(e.target.value);
  };

  const handleEmailChange = e => {
    setEmail(e.target.value);
    // console.log(e.target.value);
  };

  const handlePasswordChange = e => {
    setPassword(e.target.value);
    // console.log(e.target.value);
  };

  const handleRegister = () => {
    axios
      .post(base_url + '/register', {
        name,
        mobileNumber,
        email,
        password
      })
      .then(res => {
        console.log(res.data);
        window.location.replace('#/login');
      });
  };

  return (
    <Fragment>
      <div className='vh-100 landing-page-bg' style={{ marginTop: 75 }}>
        <Navibar />
        <div className='mt-3'>
          <h1 className='text-center text-uppercase text-white pt-5 pb-3'>Register</h1>
          <div
            className='col-lg-4 offset-lg-4 px-5 py-5 text-primary'
            style={{ backgroundColor: '#373737' }}>
            <FormInput
              label={'Name'}
              placeholder={'Enter your name'}
              type={'text'}
              onChange={handleNameChange}
            />
            <FormInput
              label={'Mobile Number'}
              placeholder={'Enter your mobile number'}
              type={'number'}
              onChange={handleMobileNumberChange}
            />
            <FormInput
              label={'Email'}
              placeholder={'Enter your email'}
              type={'email'}
              onChange={handleEmailChange}
            />
            <FormInput
              label={'Password'}
              placeholder={'Enter your password'}
              type={'password'}
              onChange={handlePasswordChange}
            />
            <Button block color='primary' onClick={handleRegister}>
              Register
            </Button>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Register;
