import React from 'react';
import { Navbar, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import { HashLink as Link } from 'react-router-hash-link';

const Navibar = () => {
  
  return (
    <div>
      <Navbar 
        fixed='top'
        light
      >
        <NavbarBrand href='/' className="text-primary">
          <img src="/ascend-yoga-logo.png" alt="person holding a sitting yoga pose" className="logo-img ml-3" />          
          <span className="ml-3" style={{fontWeight: 600}}>Ascend Yoga</span>
        </NavbarBrand>
        <Nav className='mx-auto' style={{paddingLeft: 100}}>
          <NavItem>
            <NavLink className='text-primary text-uppercase' href='#'>
              <Link
              to="/#home"
              smooth
              >
                Home
              </Link>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className='text-primary text-uppercase'>
              <Link
              to="/#ourClasses"
              smooth
              >
                Our Classes
              </Link>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className='text-primary text-uppercase' href='#'>
            <Link
              to="/#aboutUs"
              smooth
              >
                About Us
              </Link>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className='text-primary text-uppercase' href='#'>
            <Link
              to="/#pricing"
              smooth
              >
                Pricing
              </Link>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className='text-primary text-uppercase' href='#'>
            <Link
              to="/#contactUs"
              smooth
              >
                Contact Us
              </Link>
            </NavLink>
          </NavItem>
        </Nav>
        <Nav className='ml-auto'>        
          <NavItem>
            <NavLink className='text-secondary text-uppercase' href='#/login'>
              Login
            </NavLink>
          </NavItem>
        </Nav>
      </Navbar>
    </div>
  );
};

export default Navibar;
