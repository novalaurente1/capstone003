import React, { Fragment } from 'react';

const ContactUs = () => {
  return (
    <Fragment>
      <div id='contactUs' style={{ backgroundColor: '#373737' }}>
        <div className='col-lg-8 offset-lg-2 text-center pt-5'>
          <h3 className='mt-4 text-center text-primary text-uppercase'>Contact Us</h3>
          <h5 className='mt-5 text-center text-white text-uppercase'>Makati Studio</h5>
          <h4 className='text-center text-white text-uppercase'>8 632 6236 / 0917 632 6236</h4>
          <h5 className='text-center text-white'>
            399 Sen. Gil J. Puyat Ave, Makati, 1200 Metro Manila
          </h5>
        </div>

        <div 
					className='col-lg-8 offset-lg-2 d-flex'
					style={{ bottom: 5 }}	
				>
          <small className='mt-5 mb-3 mx-auto text-white'>Copyright © Ascend Yoga 2019</small>
        </div>
      </div>
    </Fragment>
  );
};

export default ContactUs;
