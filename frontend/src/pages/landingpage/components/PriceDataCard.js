import React, { Fragment } from 'react';
import { Card, CardText, CardBody } from 'reactstrap';
import { Link } from 'react-router-dom';

const PriceDataCard = props => {
  return (
    <Fragment>
      <div className='col-lg-3 my-3 mx-3'>
        <Card className='card bg-primary' style={{ borderRadius: 50 }}>
          <CardBody className='card-body mx-auto'>
            <CardText className='pr-name'>{props.packageName}</CardText>
            <CardText className='pr-price py-0 my-0'>{props.packagePrice}</CardText>
          </CardBody>
          <Link
            className='btn col-lg-4 pr-btn mx-auto mb-3 text-uppercase'
            style={{ borderRadius: 50 }}
            to='register'>
            Avail
          </Link>
        </Card>
      </div>
    </Fragment>
  );
};

export default PriceDataCard;
