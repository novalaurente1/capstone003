import React, { Fragment } from 'react';
import { Navibar } from '../../components';
import { Link } from 'react-router-dom';
import { OurClasses, AboutUs, Pricing, ContactUs } from '.';

const Home = () => {
  return (
    <Fragment>
      <div id='home' className='vh-100 landing-page-bg' style={{paddingTop: 75}}>
        <div className='col-lg-10 offset-lg-1 text-center' style={{paddingTop: 100}}>
          <p className='lp-subtitle'>Achieve your highest form</p>
          <p className='lp-subtitle'>here at</p>
          <p className='lp-companyname text-uppercase'>Ascend Yoga</p>
          <Link
            to='/register'
            className='btn lp-btn text-uppercase mt-3'
            style={{ paddingTop: 12.5 }}>
            Enroll Now
          </Link>
        </div>
      </div>
    </Fragment>
  );
};

export default Home;
