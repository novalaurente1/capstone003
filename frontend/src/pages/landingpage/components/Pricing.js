import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { PriceDataCard } from '.';

const Pricing = () => {
  return (
    <Fragment>
      <div id='pricing' className='vh-75' style={{ paddingTop: 75 }}>
        <div className='col-lg-10 offset-lg-1 text-center mt-5 mb-5'>
          <h3 className='text-center text-primary text-uppercase'>
            Pricing
          </h3>
          <div className='d-flex flex-wrap justify-content-center mt-3'>
            <PriceDataCard packageName={'1 Class'} packagePrice={'₱ 399.00'} />
            <PriceDataCard packageName={'3 Classes'} packagePrice={'₱ 1,197.00'} />
            <PriceDataCard packageName={'6 Classes'} packagePrice={'₱ 2,394.00'} />
            <PriceDataCard packageName={'9 Classes'} packagePrice={'₱ 3,591.00'} />
            <PriceDataCard packageName={'20 Classes'} packagePrice={'₱ 5,000.00'} />
            <PriceDataCard packageName={'50 Classes'} packagePrice={'₱ 15,000.00'} />
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Pricing;
