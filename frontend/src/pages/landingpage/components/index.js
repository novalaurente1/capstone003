import Home from './Home';
import OurClasses from './OurClasses';
import ClassDataCard from './ClassDataCard';
import PriceDataCard from './PriceDataCard';
import AboutUs from './AboutUs';
import Pricing from './Pricing';
import ContactUs from './ContactUs';

export {
  Home,
  OurClasses,
  ClassDataCard,
  AboutUs,
  Pricing,
  PriceDataCard,
  ContactUs
}