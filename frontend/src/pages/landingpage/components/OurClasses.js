import React, { Fragment } from 'react';
import { ClassDataCard } from '.';

const OurClasses = () => {
  return (
    <Fragment>
      <div id='ourClasses' className='vh-100' style={{paddingTop: 75}}>
          <div className='col-lg-10 offset-lg-1 text-center mt-5'>
            <h3 className='text-center text-primary text-uppercase'>Our Classes</h3>
            <div className='d-flex flex-wrap justify-content-center'>
              <ClassDataCard
                backgroundImage={'/yogapose1.jpg'}
                yogaClassName={'Fundamentals'}
                yogaClassDesc={
                  'Learn about yoga basics with expert tips + advice to help you start your practice on the right foot. Strongly recommended for beginners.'
                }
              />
              <ClassDataCard
                backgroundImage={'/yogapose2.jpg'}
                yogaClassName={'Yin'}
                yogaClassDesc={
                  'A slow-paced style of yoga as exercise, incorporating principles of Traditional Chinese Medicine, with asanas (postures) that are held for longer periods of time.'
                }
              />
              <ClassDataCard
                backgroundImage={'/yogapose3.jpg'}
                yogaClassName={'Ashtanga'}
                yogaClassDesc={
                  'Great for building core strength and toning the body. Prepare to sweat as you briskly move through a set sequence.'
                }
              />
              <ClassDataCard
                backgroundImage={'/yogapose4.jpg'}
                yogaClassName={'Vinyasa'}
                yogaClassDesc={
                  'A style of yoga characterized by stringing postures together so that you move from one to another, seamlessly, using breath. Commonly referred to as “flow” yoga.'
                }
              />
              <ClassDataCard
                backgroundImage={'/yogapose5.jpg'}
                yogaClassName={'Hot Yoga'}
                yogaClassDesc={
                  'A form of yoga as exercise performed under hot and humid conditions. It’s an intense workout that will provide strength, flexibility and tone.'
                }
              />
            </div>
          </div>
      </div>
    </Fragment>
  );
};

export default OurClasses;
