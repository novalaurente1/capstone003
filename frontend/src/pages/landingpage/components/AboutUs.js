import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { ClassDataCard } from '.';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { Carousel } from 'react-responsive-carousel';

const AboutUs = () => {
  return (
    <Fragment>
      <div id='aboutUs' className='vh-100' style={{ backgroundColor: '#373737', paddingTop: 75 }}>
          <div className='col-lg-10 offset-lg-1 text-center mt-5'>
            <h3 className='text-center text-primary text-uppercase'>
              About Us
            </h3>
            <h5 className='col-lg-8 mx-auto my-5 text-center text-white'>
              We embrace the eight limbs of yoga in everything we do; valuing honesty, compassion,
              integrity, and standing up for our beliefs. We also value silliness, fun, and not
              taking life too seriously. We deeply appreciate giving back and sharing knowledge.
            </h5>
            <div className='h-50 col-lg-5 mx-auto d-flex justify-content-center my-3'>
              <Carousel showThumbs={false}>
                <div>
                  <img src='/yogastudio1.jpg' />
                </div>
                <div>
                  <img src='/yogastudio2.jpg' />
                </div>
                <div>
                  <img src='/yogastudio3.jpg' />
                </div>
              </Carousel>
            </div>
        </div>
      </div>
    </Fragment>
  );
};

export default AboutUs;
