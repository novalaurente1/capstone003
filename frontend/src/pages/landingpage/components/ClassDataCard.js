import React, { Fragment } from 'react';
import { Card, CardTitle, CardText, CardImg, CardImgOverlay } from 'reactstrap';
import { Link } from 'react-router-dom';

const ClassDataCard = props => {
  return (
    <Fragment>
      <div className='col-lg-4 my-3'>
        <Card inverse>
          <CardImg width='100%' src={props.backgroundImage} alt='Card image cap' />
          <CardImgOverlay style={{ backgroundColor: 'rgba(55, 55, 55, 0.75)' }}>
            <CardTitle className="card-title text-left">{props.yogaClassName}</CardTitle>
            <CardText className="text-left">
            {props.yogaClassDesc}
            </CardText>
            <Link
              className='btn btn-primary mt-1 px-4 text-uppercase'
              style={{borderRadius: 50}}
              to='register'
            >
            Enroll
            </Link>
          </CardImgOverlay>
        </Card>
      </div>
    </Fragment>
  );
};

export default ClassDataCard;
