import React, { Fragment } from 'react';
import { Navibar } from '../components';
import { Home, OurClasses, AboutUs, Pricing, ContactUs } from './components';

const LandingPage = () => {
  return (
    <Fragment>    
        <Navibar />
        <Home />
        <OurClasses />
        <AboutUs />
        <Pricing />
        <ContactUs />
    </Fragment>
  );
};

export default LandingPage;
