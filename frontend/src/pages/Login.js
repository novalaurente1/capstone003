import React, { Fragment, useState } from 'react';
import { FormInput } from '../components';
import { Button } from 'reactstrap';
import axios from 'axios';
import { Navibar } from './components';

const base_url = process.env.REACT_APP_API_BASE_URL;

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleEmailChange = e => {
    setEmail(e.target.value);
    // console.log(e.target.value);
  };

  const handlePasswordChange = e => {
    setPassword(e.target.value);
    // console.log(e.target.value);
  };

  const handleLogin = () => {
    axios
      .post(base_url + '/login', {
        email,
        password
      })
      .then(res => {
        sessionStorage.token = res.data.token;
        sessionStorage.user = JSON.stringify(res.data.user);
    
        let user = res.data.user;
        
        if(user.isAdmin){
          window.location.replace('#/admin/dashboard');
        }else {
          window.location.replace('#/packages');
        }
        

      });
  };

  return (
    <Fragment>
          <div className='vh-100 landing-page-bg' style={{marginTop: 75}}>
          <Navibar />
          <div className="mt-3">
            <h1 className='text-center text-uppercase text-white pt-5 pb-3'>Login</h1>
            <div className='col-lg-4 offset-lg-4 px-5 py-5 text-primary' style={{ backgroundColor: '#373737' }}>
              <FormInput
                label={'Email'}
                placeholder={'Enter your email'}
                type={'email'}
                onChange={handleEmailChange}
              />
              <FormInput
                label={'Password'}
                placeholder={'Enter your password'}
                type={'password'}
                onChange={handlePasswordChange}
              />
              <Button block color='primary' onClick={handleLogin}>
                Login
              </Button>
            </div>
          </div>
      </div>
    </Fragment>
  );
};

export default Login;
