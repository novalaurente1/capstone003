import React from 'react';
import {
  HashRouter,
  Route,
  Switch
} from 'react-router-dom';
import { Spinner } from 'reactstrap';

const loading = () => {
  return (
  <div className="bg-primary d-flex flex-column justify-content-center align-items-center vh-100">
    <h1>Loading...</h1>
    <Spinner size="lg" color="white" />
  </div>
  )
}

const LandingPage = React.lazy(()=>import('./pages/landingpage/LandingPage'));
const Register = React.lazy(()=>import('./pages/Register'));
const Login = React.lazy(()=>import('./pages/Login'));

// USER
const Checkout = React.lazy(()=>import('./user/pages/checkout/Checkout'));
const DisplayYogaPackages = React.lazy(()=>import('./user/pages/displayyogapackages/DisplayYogaPackages'));
const DisplayAccount = React.lazy(()=>import('./user/pages/displayaccount/DisplayAccount'));
const DisplayClassSchedules = React.lazy(()=>import('./user/pages/displayclassschedules/DisplayClassSchedules'));
const DisplayEvents = React.lazy(()=>import('./user/pages/displayevents/DisplayEvents'));

// ADMIN
const Dashboard = React.lazy(()=>import('./admin/pages/dashboard/AdminDashboard'));
const Subscriptions = React.lazy(()=>import('./admin/pages/subscriptions/Subscriptions'));
const ManageClasses = React.lazy(()=>import('./admin/pages/yogaclasses/YogaClasses'));
const ManageSchedules = React.lazy(()=>import('./admin/pages/yogaclassschedules/YogaClassSchedules'));
const ManagePackages = React.lazy(()=>import('./admin/pages/yogapackages/YogaPackages'));
const ManageAttendance = React.lazy(()=>import('./admin/pages/yogaattendance/YogaAttendance'));
const ManageCustomers = React.lazy(()=>import('./admin/pages/customers/Customers'));
const ManageInstructors = React.lazy(()=>import('./admin/pages/instructors/Instructors'));

const App = () => {
  return (
    <HashRouter>
      <React.Suspense fallback={loading()}>
        <Switch>
            <Route 
              path="/"
              exact
              name="Landing Page"
              render={props => <LandingPage {...props} />}
            />
            <Route 
              path="/register"
              exact
              name="Register"
              render={props => <Register {...props} />}
            />
            <Route 
              path="/login"
              exact
              name="Login"
              render={props => <Login {...props} />}
            />
          {/* USER */}
            <Route 
              path="/checkout"
              exact
              name="Checkout"
              render={props => <Checkout {...props} />}
            />
            <Route 
              path="/packages"
              exact
              name="Yoga Packages"
              render={props => <DisplayYogaPackages {...props} />}
            />
            <Route 
              path="/myaccount"
              exact
              name="Account Details"
              render={props => <DisplayAccount {...props} />}
            />
            <Route 
              path="/schedules"
              exact
              name="Class Schedules"
              render={props => <DisplayClassSchedules {...props} />}
            />
            <Route 
              path="/events"
              exact
              name="Events"
              render={props => <DisplayEvents {...props} />}
            />
            {/* ADMIN */}
            <Route 
              path="/admin/dashboard"
              exact
              name="Subscriptions"
              render={props => <Dashboard {...props} />}
            />
            <Route 
              path="/admin/manageclasses"
              exact
              name="Manage Classes"
              render={props => <ManageClasses {...props} />}
            />
            <Route 
              path="/admin/manageschedules"
              exact
              name="Manage Schedules"
              render={props => <ManageSchedules {...props} />}
            />
            <Route 
              path="/admin/managesubscriptions"
              exact
              name="Subscriptions"
              render={props => <Subscriptions {...props} />}
            />
            <Route 
              path="/admin/managepackages"
              exact
              name="Manage Packages"
              render={props => <ManagePackages {...props} />}
            />
            <Route 
              path="/admin/managecustomers"
              exact
              name="Manage Customers"
              render={props => <ManageCustomers {...props} />}
            />
            <Route 
              path="/admin/manageattendance"
              exact
              name="Manage Attendance"
              render={props => <ManageAttendance {...props} />}
            />
            <Route 
              path="/admin/manageinstructors"
              exact
              name="Manage Instructors"
              render={props => <ManageInstructors {...props} />}
            />
        </Switch>
      </React.Suspense>
    </HashRouter>
  )
}

export default App;