import React, { Fragment } from 'react';
import { Button } from 'reactstrap';

const YogaPackageRow = props => {

  const yogaclass = props.yogaclass;

  const yogaclassId = yogaclass._id;

  const yogaclassCode = yogaclassId.substr(17,7);

  return(
    <Fragment>
      <tr> 
        <td>{yogaclassCode}</td>
        <td>{yogaclass.name}</td>
        <td>{yogaclass.description}</td>
        <td>
          <div className="d-flex">
            <Button
                className="mr-3"
                color="info"
            >
              Edit
            </Button>
            <Button
              color="danger"
              onClick={()=>props.handleDeleteClass(yogaclass._id)}
            >
              Delete
            </Button>
          </div>
        </td>
      </tr>
    </Fragment>
  )
}

export default YogaPackageRow;