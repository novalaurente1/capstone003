import YogaClassRow from './YogaClassRow';
import YogaClassAddForm from './YogaClassAddForm';

export {
  YogaClassRow,
  YogaClassAddForm
}