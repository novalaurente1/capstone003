import React, {useState} from 'react';
import { Modal, ModalHeader, ModalBody, Button } from 'reactstrap';
import { FormInput } from '../../../../components';

const YogaClassAddForm = props => {

  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggle = () => setDropdownOpen(!dropdownOpen);
   
  return (
    <Modal isOpen={props.showAddForm} toggle={props.handleShowAddForm}>
      <ModalHeader toggle={props.handleShowAddForm}>Add Class</ModalHeader>
      <ModalBody>
        <FormInput
          label={'Class Name'}
          type={'text'}
          name={'name'}
          placeholder={'Enter Class Name'}
          onChange={props.handleYogaClassNameChange}
        />
        <FormInput
          label={'Class Description'}
          type={'text'}
          name={'description'}
          placeholder={'Enter Class Type'}
          onChange={props.handleYogaClassDescriptionChange}
        />
        <div className='d-flex justify-content-end'>
          <Button
            color='primary'
            onClick={props.handleSaveYogaClass}
            disabled={
              props.name === '' ||
              props.description  === ''
                ? true
                : false
            }>
            Add Class
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};

export default YogaClassAddForm;
