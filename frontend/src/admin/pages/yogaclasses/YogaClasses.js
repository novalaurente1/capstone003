import React, { Fragment, useEffect, useState } from 'react';
import { Button } from 'reactstrap';
import { YogaClassRow, YogaClassAddForm } from './components';
import axios from 'axios';
import { Sidebar, Topbar } from '../../layout/components';

const YogaClasses = () => {
  const base_url = process.env.REACT_APP_API_BASE_URL + '/admin';
  const [yogaclasses, setYogaClasses] = useState([]);
  const [showAddForm, setShowAddForm] = useState(false);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');

  const handleRefresh = () => {
    setShowAddForm(false);
    setName('');
    setDescription('');
  };

  useEffect(() => {
    axios.get(base_url + '/showyogaclasses').then(res => {
      setYogaClasses(res.data);
    });
  }, []);

  const handleShowAddForm = () => {
    setShowAddForm(!showAddForm);
  };

  const handleDeleteClass = yogaclassId => {
    axios({
      method: 'DELETE',
      url: base_url + '/deleteyogaclass/' + yogaclassId
    }).then(res => {
      let newYogaClasses = yogaclasses.filter(yogaclass => yogaclass._id !== yogaclassId);
      setYogaClasses(newYogaClasses);
    });
  };

  const handleYogaClassNameChange = e => {
    // console.log(e.target.value);
    setName(e.target.value);
  };

  const handleYogaClassDescriptionChange = e => {
    // console.log(e.target.value);
    setDescription(e.target.value);
  };

  const handleSaveYogaClass = () => {
    axios({
      method: 'POST',
      url: base_url + '/addyogaclass',
      data: {
        name: name,
        description: description
      }
    }).then(res => {
      let newYogaClasses = [...yogaclasses];
      newYogaClasses.push(res.data);
      setYogaClasses(newYogaClasses);
      handleRefresh();
    });
  };

  return (
    <Fragment>
      <div className='d-flex'>
        <Sidebar />
        <div className='d-flex flex-column w-100'>
          <Topbar />
          <div className='content'>
            <h1 className='col-lg-10 text-primary mt-5 py-3 ml-5'>Classes</h1>
            <div className='col-lg-10 justify-content-start mb-3 ml-5'>
              <Button color='primary' onClick={handleShowAddForm}>
                Add Class
              </Button>
              <YogaClassAddForm
                showAddForm={showAddForm}
                handleShowAddForm={handleShowAddForm}
                handleYogaClassNameChange={handleYogaClassNameChange}
                handleYogaClassDescriptionChange={handleYogaClassDescriptionChange}
                name={name}
                description={description}
                handleSaveYogaClass={handleSaveYogaClass}
              />
              <table className='table table-striped border my-3'>
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {yogaclasses.map(yogaclass => (
                    <YogaClassRow
                      key={yogaclass._id}
                      yogaclass={yogaclass}
                      handleDeleteClass={handleDeleteClass}
                    />
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default YogaClasses;
