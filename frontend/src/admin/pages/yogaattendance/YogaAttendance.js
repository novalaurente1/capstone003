import React, { Fragment, useState, useEffect } from 'react';
import { Sidebar, Topbar } from '../../layout/components';
import { YogaAttendanceRow } from './components';
import { Link } from 'react-router-dom';
import axios from 'axios';

const YogaAttendance = props => {
  const base_url = process.env.REACT_APP_API_BASE_URL; 
  const [attendance, setAttendance] = useState([]);
  const [attendanceStatus, setAttendanceStatus] = useState("Booked");

  useEffect(() => {
    if(props.location.state !== undefined) {
      const classSchedId = props.location.state.classSchedId;
      
      axios.get(base_url + '/showattendancebyschedule/' + classSchedId)
        .then(res=>{
          setAttendance(res.data);
        })
    }
  }, []);

  console.log(attendance);

  const handleRemoveStudent = attendanceId => {
    axios({
      method: 'DELETE',
      url: base_url + '/deletestudent/' + attendanceId
    }).then(res => {
      let newAttendance = attendance.filter(attendance => attendance._id !== attendanceId);
      setAttendance(newAttendance);
    });
  };

  const handleAttendanceStatus = (attendanceId, attendanceStatus) => {
    
    axios.patch(base_url + '/updateattendance/' + attendanceId, {
			attendance: attendanceStatus
		}).then(res=>{
      let index = attendance.findIndex(attd => attd._id === attendanceId);

      let newAttendances = [...attendance];
      
			newAttendances.splice(index, 1, res.data);
      setAttendance(newAttendances);

		})
  }

  if (props.location.state == undefined) {
    return <div>Class does not exist</div>;
  } else {
    const { classDate, classStartTime, className, classInstructor } = props.location.state;

    return (
      <Fragment>
        <div className='d-flex'>
          <Sidebar />
          <div className='d-flex flex-column w-100'>
            <Topbar />
            <div className='content'>
              <div className='mt-3 ml-3'>
                <Link className='text-secondary' to='/admin/manageschedules'>
                  <i className='fa fa-angle-double-left' aria-hidden='true'>
                    <span style={{ fontFamily: 'Ubuntu' }}> Return to Class Schedules</span>
                  </i>
                </Link>
              </div>
              <h3 className='col-lg-10 text-primary mt-5 py-3 ml-5'>
                Attendance for:{' '}
                <span className='text-dark'>
                  {classDate} {classStartTime}, {className}, {classInstructor}
                </span>
              </h3>
              <div className='col-lg-10 justify-content-start mb-3 ml-5'>
                <table className='table table-striped border my-3'>
                  <thead>
                    <tr>
                      <th>Customer Code</th>
                      <th>Customer Name</th>
                      <th>Attendance</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                  {attendance.map(attendance=>
                    <YogaAttendanceRow
                      key={attendance._id}
                      attendance={attendance}
                      handleRemoveStudent={handleRemoveStudent}
                      handleAttendanceStatus={handleAttendanceStatus}
                    />
                  )}

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
};

export default YogaAttendance;
