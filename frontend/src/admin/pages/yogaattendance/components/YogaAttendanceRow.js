import React, { Fragment, useState } from 'react';
import { 
  Button,
  Dropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem
 } from 'reactstrap';

const YogaPackageRow = props => {

  const attendance = props.attendance;

  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggle = () => {
    setDropdownOpen(!dropdownOpen);
  }

  const customerId = attendance.userId;

  const customerCode = customerId.substr(17,7); 

  return(
    <Fragment>
      <tr>
        <td>{customerCode}</td>
        <td>{attendance.userName}</td>
        <td>
          <Dropdown
            isOpen={dropdownOpen}
            toggle={toggle}
          >
            <DropdownToggle>
              {attendance.attendance}&nbsp;&nbsp; 
              <i className="fa fa-caret-down" aria-hidden="true"></i>
            </DropdownToggle>
            <DropdownMenu>
              <DropdownItem
                onClick={()=>props.handleAttendanceStatus(attendance._id, "Booked")}
              >Booked          
              </DropdownItem>
              <DropdownItem
                onClick={()=>props.handleAttendanceStatus(attendance._id, "Present")}
              >Present
              </DropdownItem>
              <DropdownItem
                onClick={()=>props.handleAttendanceStatus(attendance._id, "Absent")}
              >Absent
                </DropdownItem>
            </DropdownMenu>	
          </Dropdown>

        </td>
        <td>
          <div className="d-flex">
            <Button
              color="danger"
              onClick={()=>props.handleRemoveStudent(attendance._id)}
            >
              Remove
            </Button>
          </div>
        </td>
      </tr>
    </Fragment>
  )
}

export default YogaPackageRow;