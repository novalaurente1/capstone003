import React, { Fragment, useEffect, useState } from 'react';
import { CustomerRow } from './components';
import axios from 'axios';
import { Sidebar, Topbar } from '../../layout/components';

const Customers = () => {
  const base_url = process.env.REACT_APP_API_BASE_URL + '/admin';
  const [customers, setCustomers] = useState([]);
  
  useEffect(() => {
    axios.get(base_url + '/showusers').then(res => {
      setCustomers(res.data);
    });
  }, []);

  return (
    <Fragment>
      <div className='d-flex'>
        <Sidebar />
        <div className='d-flex flex-column w-100'>
          <Topbar />
          <div className='content'>
            <h1 className='col-lg-10 text-primary mt-5 py-3 ml-5'>Customers</h1>
            <div className='col-lg-10 justify-content-start mb-3 ml-5'>
              <table className='table table-striped border my-3'>
                <thead>
                  <tr>
                    <th>Date Registered</th>
                    <th>Code</th>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Mobile No.</th>
                    <th>Email Address</th>
                  </tr>
                </thead>
                <tbody>
                  {customers.map(customer => (
                    <CustomerRow
                      key={customer._id}
                      customer={customer}
                    />
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Customers;
