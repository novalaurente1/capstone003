import React, { Fragment } from 'react';
import { Button } from 'reactstrap';

const CustomerRow = props => {

  const base_url = process.env.REACT_APP_API_BASE_URL;
  
  const customer = props.customer;

  const customerId = customer._id;

  const customerCode = customerId.substr(17,7); 

  return(
    <Fragment>
      <tr> 
        <td>{customer.dateRegistered}</td>
        <td>{customerCode}</td>
        <td>{customer.name}</td>
        <td>
        {customer.profilePicture === '' 
        ? <p>No photo uploaded</p>
        : <img src={base_url + "/images/uploads/" + customer.profilePicture} height="75px" />
        }
        </td>
        <td>{customer.mobileNumber}</td>
        <td>{customer.email}</td>
      </tr>
    </Fragment>
  )
}

export default CustomerRow;