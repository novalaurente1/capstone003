import React, {useEffect, useState, Fragment} from 'react';
import { Sidebar, Topbar } from '../../layout/components';
import { DataCard, LongDataCard } from './components';
import axios from 'axios';

const AdminDashboard = () => {
  const base_url = process.env.REACT_APP_API_BASE_URL + '/admin'
	const [user, setUser] = useState({});
	const [totalSchedules, setTotalSchedules] = useState(0);
	const [totalCustomers, setTotalCustomers] = useState(0);
	const [totalPackages, setTotalPackages] = useState(0);
	
	useEffect(()=>{
		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user);
			setUser(user);
		}else{
			window.location.replace("#/login");
		}
	}, [])

	useEffect(()=>{
		axios.get(base_url + '/countschedules').then(res => {
			setTotalSchedules(res.data);
    });
	}, [])

	useEffect(()=>{
		axios.get(base_url + '/countusers').then(res => {
			setTotalCustomers(res.data);
    });
	}, [])

	useEffect(()=>{
		axios.get(base_url + '/countpackages').then(res => {
			// console.log(res.data)
			setTotalPackages(res.data);
    });
	}, [])
	
	// console.log(totalCustomers.totalCustomers);

	return (
		<Fragment>

			<div className='d-flex'>

        <Sidebar />
        <div className='d-flex flex-column w-100'>
				<Topbar />
          <div className='content'>
					<h3 className="col-lg-10 offset-lg-1 text-center pt-3">Welcome, {user.name}</h3>
						<div className="col-lg-10 offset-lg-1 row d-flex mt-4">
							<LongDataCard />
						</div>
						<div className="col-lg-10 offset-lg-1 row d-flex mt-4">
							<DataCard 
								backgroundColor="#373737" 
								moduleName={"Class Schedules"}
								moduleLink={"/admin/manageschedules"}
								moduleCount={totalSchedules.totalSchedules}
							/>
							<DataCard 
								backgroundColor="#B3004C" 
								moduleName={"Customers"}
								moduleLink={"/admin/managecustomers"}
								moduleCount={totalCustomers.totalCustomers}
							/>
							<DataCard 
								backgroundColor="#451C40" 
								moduleName={"Packages"}
								moduleLink={"/admin/managepackages"}
								moduleCount={totalPackages.totalPackages}
							/>
						</div>
					</div>
				</div>         
			</div>
    </Fragment>
	)
}

export default AdminDashboard;