import React, { Fragment, useState, useEffect } from 'react';
import {
  Card, CardText, CardBody
} from 'reactstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';

const LongDataCard = () => {
  const base_url = process.env.REACT_APP_API_BASE_URL
  const [revenue, setRevenue] = useState(0);

  useEffect(() => {
    axios.get(base_url + '/revenue').then(res => {
      setRevenue(res.data.revenue);
    });
  }, []);

  return (
    <Fragment>
    <div
      className="col-lg-12 stretch-card grid-margin"
    >
      <Card
        className="card bg-primary text-white"
      >
        <CardBody
          className="card-body ml-4 mr-auto d-flex align-items-center"
        >
          <CardText
            className="text-white mt-3"
            style={{fontSize: 24, fontWeight: 500, letterSpacing: 0.15}}
          >Revenue</CardText>
          <Link
            className="text-white ml-3"
            style={{fontSize: 64}}
            to="/admin/managesubscriptions"
          >
          {revenue}
          </Link>
           
        </CardBody>
      </Card>
    </div>
    </Fragment>
  )
}

export default LongDataCard;