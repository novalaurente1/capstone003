import DataCard from './DataCard';
import LongDataCard from './LongDataCard';

export {
  DataCard,
  LongDataCard
}