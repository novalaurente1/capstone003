import React, { Fragment } from 'react';
import {
  Card, CardText, CardBody
} from 'reactstrap';
import { Link } from 'react-router-dom';

const DataCard = (props) => {
  const backgroundColor = props.backgroundColor;
  const moduleLink = props.moduleLink;

  return (
    <Fragment>
    <div
      className="col-lg-4 stretch-card grid-margin"
    >
      <Card
        className="card text-white mb-4"
        style={{
          backgroundColor: backgroundColor
        }}
      >
        <CardBody
          className="card-body pl-5 pb-0 mr-auto"
        >
          <CardText
            style={{fontSize: 24}}     
          >
          {props.moduleName}
          </CardText>
          <CardText
            className="py-0 my-0"
            style={{fontSize: 64}}     
          >
          {props.moduleCount}
          </CardText>
        </CardBody>
        <Link
          className="text-white ml-auto px-4 pb-2"
          style={{fontSize: 48, marginTop: -50}}
          to={{pathname: moduleLink}}
        >
        <i className="fas fa-plus-circle"></i>
        </Link>
      </Card>
    </div>
    </Fragment>
  )
}

export default DataCard;