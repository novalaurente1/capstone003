import React, { Fragment, useEffect, useState } from 'react';
import { Button } from 'reactstrap';
import { YogaPackageRow, YogaPackageAddForm, YogaPackageEditForm } from './components';
import axios from 'axios';
import { Sidebar, Topbar } from '../../layout/components';
import { Link } from 'react-router-dom';

const YogaPackages = () => {
  const base_url = process.env.REACT_APP_API_BASE_URL;
  const [yogapackages, setYogaPackages] = useState([]);
  const [showAddForm, setShowAddForm] = useState(false);
  const [name, setName] = useState('');
  const [type, setType] = useState('Limited');
  const [numberOfSessions, setNumberOfSessions] = useState(0);
  const [price, setPrice] = useState(0);
  const [showEditForm, setShowEditForm] = useState(false);
  const [editYogaPackage, setEditYogaPackage] = useState({});
  const [edit, setEdit] = useState(true);

  const handleRefresh = () => {
    setShowAddForm(false);
    setShowEditForm(false);
    setName('');
    setType('');
    setNumberOfSessions(0);
    setPrice(0);
  };

  useEffect(() => {
    axios.get(base_url + '/admin/showyogapackages').then(res => {
      const responseData = res.data;

      setYogaPackages(responseData);
    });
  }, []);

  const handleShowAddForm = () => {
    setShowAddForm(!showAddForm);
  };

  const handleShowEditForm = yogapackageId => {
    axios.get(base_url + '/admin/showyogapackage/' + yogapackageId).then(res => {
      setEditYogaPackage(res.data);
      setShowEditForm(!showEditForm);
      setEdit(true);
    });
  }; 

  const toggleEditForm = () => {
    setShowEditForm(!showEditForm)
  }

  const handleDeletePackage = yogapackageId => {
    axios({
      method: 'DELETE',
      url: base_url + '/admin/deleteyogapackage/' + yogapackageId
    }).then(res => {
      let newYogaPackages = yogapackages.filter(yogapackage => yogapackage._id !== yogapackageId);
      setYogaPackages(newYogaPackages);
    });
  };

  const handlePackageNameChange = e => {
    setName(e.target.value);
  };

  const handlePackageTypeChange = e => {
    setType(e.target.value);
  };

  const handlePackageSessionChange = e => {
    setNumberOfSessions(e.target.value);
  };

  const handlePackagePriceChange = e => {
    setPrice(e.target.value);
  };

  const handleSaveYogaPackage = () => {
    axios({
      method: 'POST',
      url: base_url + '/admin/addyogapackage',
      data: {
        name: name,
        type: type,
        numberOfSessions: parseInt(numberOfSessions),
        price: parseInt(price)
      }
    }).then(res => {
      let newYogaPackages = [...yogapackages];
      newYogaPackages.push(res.data);
      setYogaPackages(newYogaPackages);
      handleRefresh();
    });
  };

  const handlePackageNameUpdate = e => {
    setName(e.target.value);
  };

  const handlePackageTypeUpdate = e => {
    setType(e.target.value);
  };

  const handlePackageSessionUpdate = e => {
    setNumberOfSessions(e.target.value);
  };

  const handlePackagePriceUpdate = e => {
    setPrice(e.target.value);
  };

  const handleUpdateYogaPackage = (editId) => {

    let editName = edit && name === "" ? editYogaPackage.name : name;
    let editType = edit && type === "" ? editYogaPackage.type : type;
    let editNumberOfSessions = edit && numberOfSessions === "" ? editYogaPackage.numberOfSessions : numberOfSessions;
    let editPrice = edit && price === "" ? editYogaPackage.price : price;

    axios({
      method: 'PUT',
      url: base_url + '/admin/updateyogapackage/' + editId,
      data: {
        name: editName,
        type: editType,
        numberOfSessions: parseInt(editNumberOfSessions),
        price: parseInt(editPrice)
      }
    }).then(res => {
      let oldIndex;
      yogapackages.forEach((yogapackage,index)=>{
        if(yogapackage._id === editId) {
          oldIndex = index;
        }
      });
      let newYogaPackages = [...yogapackages];
      newYogaPackages.splice(oldIndex, 1, res.data);
      setYogaPackages(newYogaPackages);
      toggleEditForm();
    });
  };

  return (
    <Fragment>
      <div className='d-flex'>
        <Sidebar />
        <div className='d-flex flex-column w-100'>
          <Topbar />
          <div className='content'>
            <h1 className='col-lg-10 text-primary mt-5 py-3 ml-5'>Packages</h1>
            <div className='col-lg-10 justify-content-start mb-3 ml-5'>
              <Button color='primary' onClick={handleShowAddForm}>
                Add Package
              </Button>
              <YogaPackageAddForm
                showAddForm={showAddForm}
                handleShowAddForm={handleShowAddForm}
                handlePackageNameChange={handlePackageNameChange}
                handlePackageTypeChange={handlePackageTypeChange}
                handlePackageSessionChange={handlePackageSessionChange}
                handlePackagePriceChange={handlePackagePriceChange}
                name={name}
                type={type}
                numberOfSessions={numberOfSessions}
                price={price}
                handleSaveYogaPackage={handleSaveYogaPackage}
              />

              <YogaPackageEditForm
                showEditForm={showEditForm}
                toggleEditForm={toggleEditForm}
                handleShowEditForm={handleShowEditForm}
                yogaPackage={editYogaPackage}
                handlePackageNameUpdate={handlePackageNameUpdate}
                handlePackageTypeUpdate={handlePackageTypeUpdate}
                handlePackageSessionUpdate={handlePackageSessionUpdate}
                handlePackagePriceUpdate={handlePackagePriceUpdate}
                handleUpdateYogaPackage={handleUpdateYogaPackage}
              />

              <table className='table table-striped border my-3'>
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Package Name</th>
                    <th>Package Type</th>
                    <th>No. of Sessions</th>
                    <th>Price</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {yogapackages.map(yogapackage => (
                    <YogaPackageRow
                      key={yogapackage._id}
                      yogapackage={yogapackage}
                      handleDeletePackage={handleDeletePackage}
                      handleShowEditForm={handleShowEditForm}
                    />
                  ))}
                </tbody>
              </table>
              <div className="d-flex justify-content-center">
              <a
                className="btn btn-outline-primary"
                href={base_url + "/admin/exportyogapackages"}
              >
                Export to CSV
              </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default YogaPackages;
