import React, {useState} from 'react';
import { Modal, ModalHeader, ModalBody, Button} from 'reactstrap';
import { FormInput } from '../../../../components';

const YogaPackageForm = props => {
  
  const yogaPackage = props.yogaPackage;

  return (
    <Modal isOpen={props.showEditForm} toggle={props.toggleEditForm}>
      <ModalHeader toggle={props.toggleEditForm}>Edit Package</ModalHeader>
      <ModalBody>
        <FormInput
          label={'Package Name'}
          type={'text'}
          name={'name'}
          defaultValue={yogaPackage.name}
          onChange={props.handlePackageNameUpdate}
        />
        <FormInput
          label={'Package Type'}
          type={'text'}
          name={'type'}
          defaultValue={yogaPackage.type}
          onChange={props.handlePackageTypeUpdate}
        />
        <FormInput
          label={'No. of Sessions'}
          type={'number'}
          name={'numberOfSessions'}
          defaultValue={yogaPackage.numberOfSessions}
          onChange={props.handlePackageSessionUpdate}
        />
        <FormInput
          label={'Price'}
          type={'number'}
          name={'price'}
          defaultValue={yogaPackage.price}
          onChange={props.handlePackagePriceUpdate}
        />
        
        <div className='d-flex justify-content-end'>
          <Button
            color='primary'
            onClick={()=>props.handleUpdateYogaPackage(yogaPackage._id)}
          >
            Update Package
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};

export default YogaPackageForm;
