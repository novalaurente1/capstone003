import React, { Fragment } from 'react';
import { Button } from 'reactstrap';

const YogaPackageRow = props => {

  const yogapackage = props.yogapackage;

  const yogapackageId = yogapackage._id;

  const yogapackageCode = yogapackageId.substr(17,7); 

  return(
    <Fragment>
      <tr>
        <td>{yogapackageCode}</td>
        <td>{yogapackage.name}</td>
        <td>{yogapackage.type}</td>
        <td>{yogapackage.numberOfSessions}</td>
        <td>{yogapackage.price}</td>
        <td>
          <div className="d-flex">
            <Button
                className="mr-3"
                color="info"
                onClick={()=>props.handleShowEditForm(yogapackage._id)}
            >
              Edit
            </Button>
            <Button
              color="danger"
              onClick={()=>props.handleDeletePackage(yogapackage._id)}
            >
              Delete
            </Button>
          </div>
        </td>
      </tr>
    </Fragment>
  )
}

export default YogaPackageRow;