import React, {useState} from 'react';
import { Modal, ModalHeader, ModalBody, Button, Label } from 'reactstrap';
import { FormInput } from '../../../../components';

const YogaPackageAddForm = props => {

  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggle = () => setDropdownOpen(!dropdownOpen);

  return (
    <Modal isOpen={props.showAddForm} toggle={props.handleShowAddForm}>
      <ModalHeader toggle={props.handleShowAddForm}>Add Package</ModalHeader>
      <ModalBody>
        <FormInput
          label={'Package Name'}
          type={'text'}
          name={'name'}
          placeholder={'Enter Package Name'}
          onChange={props.handlePackageNameChange}
        />
        <FormInput
          label={'Package Type'}
          type={'text'}
          name={'type'}
          defaultValue={props.type}
          placeholder={'Enter Package Type'}
          onChange={props.handlePackageTypeChange}
        />
        <FormInput
          label={'No. of Sessions'}
          type={'number'}
          name={'numberOfSessions'}
          placeholder={'Enter Number of Sessions'}
          onChange={props.handlePackageSessionChange}
        />
        <FormInput
          label={'Price'}
          type={'number'}
          name={'price'}
          placeholder={'Enter Package Price'}
          onChange={props.handlePackagePriceChange}
        />
        
        <div className='d-flex justify-content-end'>
          <Button
            color='primary'
            onClick={props.handleSaveYogaPackage}
            disabled={
              props.name === '' ||
              props.type === '' ||
              props.numberOfSessions === '' ||
              props.price === '' ||
              props.validity === ''
                ? true
                : false
            }>
            Add Package
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};

export default YogaPackageAddForm;
