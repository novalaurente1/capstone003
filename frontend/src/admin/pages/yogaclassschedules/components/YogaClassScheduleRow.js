import React, { Fragment, useEffect, useState } from 'react';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';

const YogaPackageRow = props => {
  const base_url = process.env.REACT_APP_API_BASE_URL;
  const schedule = props.schedule;
  const [totalAttendees, setTotalAttendees] = useState(0);

  const classId = schedule._id;

  useEffect(()=>{
		axios.get(base_url + '/countattendees/' + classId).then(res => {
			setTotalAttendees(res.data);
    });
  }, [])
  
  return(
    <Fragment>
      <tr>
        <td>{schedule.date}</td>
        <td>{schedule.startTime} - {schedule.endTime}</td>
        <td>
          <Link
            className="text-primary" 
            to={{
              pathname: "/admin/manageattendance",
              state: { 
                classSchedId: schedule._id,
                classDate: schedule.date,
                classStartTime: schedule.startTime,
                className: schedule.yogaclassName,
                classInstructor: schedule.instructorName,
              }
            }}
            
          >
            {totalAttendees.totalAttendees}/10 Update
          </Link>
        </td>
        <td>{schedule.yogaclassName}</td>
        <td>{schedule.instructorName}</td>
        <td>
          <div className="d-flex">
            <Button
                className="mr-3"
                color="info"
            >
              Edit
            </Button>
            <Button
              color="danger"
              onClick={()=>props.handleDeleteSchedule(schedule._id)}
            >
              Cancel
            </Button>
          </div>
        </td>
      </tr>
    </Fragment>
  )
}

export default YogaPackageRow;