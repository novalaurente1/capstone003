import React, { useState } from 'react';
import { Modal, ModalHeader, ModalBody, Button } from 'reactstrap';
import { FormInput } from '../../../../components';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import { formatDate, parseDate } from 'react-day-picker/moment';
import moment from 'moment';
import TimePicker from 'rc-time-picker';
import 'rc-time-picker/assets/index.css';
import ClassInput from './ClassInput';
import InstructorInput from './InstructorInput';

const YogaClassAddForm = props => {

  const format = 'h:mm a';

  const now = moment().hour(0).minute(0);

  function endTime(value) {
    console.log(value && value.format(format));
  }

  return (
    <Modal isOpen={props.showAddForm} toggle={props.handleShowAddForm}>
      <ModalHeader toggle={props.handleShowAddForm}>Add Class Schedule</ModalHeader>
      <ModalBody>
        <ClassInput 
          className = {props.yogaclassName}
          handleClassInputChange={props.handleClassInputChange}
        />
        <InstructorInput 
          instructorName = {props.instructorName}
          handleInstructorInputChange={props.handleInstructorInputChange}
        />
        <label htmlFor='Date'>Class Date</label>
        <br />
        <DayPickerInput
          formatDate={formatDate}
          parseDate={parseDate}
          placeholder={`${formatDate(new Date())}`}
          onDayChange={props.handleDateChange}
        />
        <br />
        <br />
        <label htmlFor='Time'>Class Time</label>
        <br />
        <div className="d-flex my-auto align-items-center">
          <label className="mb-0 font-weight-bold" htmlFor='startTime'>Start</label>&nbsp;
          <TimePicker
            showSecond={false}
            defaultValue={now}
            className='xxx'
            onChange={props.handleStartTimeChange}
            format={format}
            use12Hours
            inputReadOnly
          />
          <p className="my-0">&nbsp;-&nbsp;</p>
          <label className="mb-0 font-weight-bold" htmlFor='endTime'>End</label>&nbsp;
          <TimePicker
            showSecond={false}
            defaultValue={now}
            className='xxx'
            onChange={props.handleEndTimeChange}
            format={format}
            use12Hours
            inputReadOnly
          />
        </div>
        <br />
        <div className='d-flex justify-content-end'>
          <Button
            color='primary'
            onClick={props.handleSaveYogaClass}
          >
            Add Schedule
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};

export default YogaClassAddForm;
