import React, { useState, useEffect } from 'react';
import { FormGroup, Label, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import axios from 'axios';

const ClassInput = props => {
  const base_url = process.env.REACT_APP_API_BASE_URL + '/admin/';
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [yogaclasses, setYogaclasses] = useState([]);

  const toggle = () => {
    setDropdownOpen(!dropdownOpen);
  };

  useEffect(() => {
    axios.get(base_url + 'showyogaclasses').then(res => {
      setYogaclasses(res.data);
    });
  }, []);

  return (
    <FormGroup>
      <Label>Classes </Label>
      <Dropdown isOpen={dropdownOpen} toggle={toggle}>
        <DropdownToggle>
          {props.className === '' ? 'Choose Class' : props.className}
          &nbsp;&nbsp;
          <i className='fa fa-caret-down' aria-hidden='true'></i>
        </DropdownToggle>
        <DropdownMenu>
          {yogaclasses.map(yogaclass => 
            <DropdownItem
              key={yogaclass._id}
              onClick={()=>props.handleClassInputChange(yogaclass.name, yogaclass._id)}
            >
              {yogaclass.name}
            </DropdownItem>
          )}
        </DropdownMenu>
      </Dropdown>
    </FormGroup>
  );
};

export default ClassInput;
