import React, { useState, useEffect } from 'react';
import { FormGroup, Label, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import axios from 'axios';

const InstructorInput = props => {
  const base_url = process.env.REACT_APP_API_BASE_URL + '/admin/';
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [instructors, setInstructors] = useState([]);

  const toggle = () => {
    setDropdownOpen(!dropdownOpen);
  };

  useEffect(() => {
    axios.get(base_url + 'showinstructors').then(res => {
      setInstructors(res.data);
    });
  }, []);

  return (
    <FormGroup>
      <Label>Instructors </Label>
      <Dropdown isOpen={dropdownOpen} toggle={toggle}>
        <DropdownToggle>
          {props.instructorName === '' ? 'Choose Instructor' : props.instructorName}
          &nbsp;&nbsp;
          <i className='fa fa-caret-down' aria-hidden='true'></i>
        </DropdownToggle>
        <DropdownMenu>
          {instructors.map( instructor => 
            <DropdownItem
              key={instructor._id}
              onClick={()=>props.handleInstructorInputChange(instructor.name, instructor._id)}
            >
              {instructor.name}
            </DropdownItem>
          
          )}
        </DropdownMenu>
      </Dropdown>
    </FormGroup>
  );
};

export default InstructorInput;
