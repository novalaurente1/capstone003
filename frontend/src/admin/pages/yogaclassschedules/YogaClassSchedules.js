import React, { Fragment, useEffect, useState } from 'react';
import { Button } from 'reactstrap';
import { YogaClassScheduleRow, YogaClassScheduleAddForm } from './components';
import axios from 'axios';
import { Sidebar, Topbar } from '../../layout/components';
import moment from 'moment';


const YogaClassSchedules = () => {
  const base_url = process.env.REACT_APP_API_BASE_URL + '/admin';
  const [schedules, setSchedules] = useState([]);
  const [showAddForm, setShowAddForm] = useState(false);
  const [yogaclassName, setYogaClassName] = useState('');
  const [yogaclassId, setYogaClassId] = useState('');
  const [instructorName, setInstructorName] = useState('');
  const [instructorId, setInstructorId] = useState('');
  const [date, setDate] = useState('');
  const [startTime, setStartTime] = useState('');
  const [endTime, setEndTime] = useState('');

  const handleRefresh = () => {
    setShowAddForm(false);
    setYogaClassName('');
    setInstructorName('');
    setStartTime('');
    setEndTime('');
  };

  useEffect(() => {
    axios.get(base_url + '/showschedules').then(res => {
      setSchedules(res.data);
    });
  }, []);

  const handleShowAddForm = () => {
    setShowAddForm(!showAddForm);
  };

  const handleDeleteSchedule = scheduleId => {
    axios({
      method: 'DELETE',
      url: base_url + '/deleteschedule/' + scheduleId
    }).then(res => {
      let newClassSchedules = schedules.filter(schedule => schedule._id !== scheduleId);
      setSchedules(newClassSchedules);
    });
  };

  const handleClassInputChange = (classNameInput, yogaclassId) => {
    setYogaClassName(classNameInput);
    setYogaClassId(yogaclassId);
  };

  const handleInstructorInputChange = (instructorInput, instructorId) => {
    setInstructorName(instructorInput);
    setInstructorId(instructorId);
  };

  const handleDateChange = day => {
    let date = moment(day).format('LL');
    setDate(date);
  }

  const handleStartTimeChange = value => {
    const format = 'h:mm a';
    setStartTime(value && value.format(format));
  }

  const handleEndTimeChange = value => {
    const format = 'h:mm a';
    setEndTime(value && value.format(format));
  }

  const handleSaveYogaClass = () => {
    axios({
      method: 'POST',
      url: base_url + '/addschedule',
      data: {
        yogaclassName,
        yogaclassId,
        instructorName,
        instructorId,
        date,
        startTime,
        endTime
      }
    }).then(res => {
      let newClassSchedules = [...schedules];
      newClassSchedules.push(res.data);
      setSchedules(newClassSchedules);
      handleRefresh();
    });
  };

  return (
    <Fragment>
      <div className='d-flex'>
        <Sidebar />
        <div className='d-flex flex-column w-100'>
          <Topbar />
          <div className='content'>
          <h1 className='col-lg-10 text-primary mt-5 py-3 ml-5'>Class Schedules</h1>
            <div className='col-lg-10 justify-content-start mb-3 ml-5'>
              <Button color='primary' onClick={handleShowAddForm}>
                Add Class Schedule
              </Button>
              <YogaClassScheduleAddForm
                showAddForm={showAddForm}
                handleShowAddForm={handleShowAddForm}
                handleClassInputChange={handleClassInputChange}
                handleInstructorInputChange={handleInstructorInputChange}
                handleDateChange={handleDateChange}
                handleStartTimeChange={handleStartTimeChange}
                handleEndTimeChange={handleEndTimeChange}
                yogaclassName={yogaclassName}
                instructorName={instructorName}
                startTime={startTime}
                endTime={endTime}
                handleSaveYogaClass={handleSaveYogaClass}
              />
              <table className='table table-striped border my-3'>
                <thead>
                  <tr>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Attendance</th>
                    <th>Class</th>
                    <th>Instructor</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {schedules.map(schedule => (
                    <YogaClassScheduleRow
                      key={schedule._id}
                      schedule={schedule}
                      handleDeleteSchedule={handleDeleteSchedule}
                    />
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default YogaClassSchedules;
