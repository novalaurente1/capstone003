import React, {useState} from 'react';
import { Modal, ModalHeader, ModalBody, Button } from 'reactstrap';
import { FormInput } from '../../../../components';
import Images from './Images';

const InstructorAddForm = props => {
     
  return (
    <Modal isOpen={props.showAddForm} toggle={props.handleShowAddForm}>
      <ModalHeader toggle={props.handleShowAddForm}>Add Instructor</ModalHeader>
      <ModalBody>
        <FormInput
          label={'Instructor Name'}
          type={'text'}
          name={'name'}
          placeholder={'Enter Instructor Name'}
          onChange={props.handleInstructorNameChange}
        />
        <FormInput
          label={'Instructor Description'}
          type={'text'}
          name={'description'}
          placeholder={'Enter Instructor Description'}
          onChange={props.handleInstructorDescriptionChange}
        />
        <label htmlFor="image">Instructor Image</label>
        <Images setImage={props.setImage} />
        <div className='d-flex justify-content-end'>
          <Button
            color='primary'
            onClick={props.handleSaveInstructor}
            disabled={
              props.name === '' ||
              props.description  === ''
                ? true
                : false
            }>
            Add Class
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};

export default InstructorAddForm;
