import InstructorRow from './InstructorRow';
import InstructorAddForm from './InstructorAddForm';

export {
  InstructorRow,
  InstructorAddForm
}