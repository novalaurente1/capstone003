import React, { Fragment } from 'react';
import { Button } from 'reactstrap';

const YogaPackageRow = props => {

  const instructor = props.instructor;
  const base_url = process.env.REACT_APP_API_BASE_URL;

  return(
    <Fragment>
      <tr> 
        <td>{instructor.name}</td>
        <td><img src={base_url + "/images/uploads/" + instructor.image} height="75px" /></td>
        <td>{instructor.description}</td>
        <td>
          <div className="d-flex">
            <Button
                className="mr-3"
                color="info"
            >
              Edit
            </Button>
            <Button
              color="danger"
              onClick={()=>props.handleDeleteInstructor(instructor._id)}
            >
              Delete
            </Button>
          </div>
        </td>
      </tr>
    </Fragment>
  )
}

export default YogaPackageRow;