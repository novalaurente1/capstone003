import React, { Fragment, useEffect, useState } from 'react';
import { Button } from 'reactstrap';
import { InstructorRow, InstructorAddForm } from './components';
import axios from 'axios';
import { Sidebar, Topbar } from '../../layout/components';

const Instructors = () => {
  const base_url = process.env.REACT_APP_API_BASE_URL + '/admin';
  const [instructors, setInstructors] = useState([]);
  const [showAddForm, setShowAddForm] = useState(false);
  const [name, setName] = useState('');
  const [image, setImage] = useState('');
  const [description, setDescription] = useState('');

  const handleRefresh = () => {
    setShowAddForm(false);
    setName('');
    setImage('');
    setDescription('');
  };

  useEffect(() => {
    axios.get(base_url + '/showinstructors').then(res => {
      setInstructors(res.data);
    });
  }, []);

  const handleShowAddForm = () => {
    setShowAddForm(!showAddForm);
  };

  const handleDeleteInstructor = instructorId => {
    axios({
      method: 'DELETE',
      url: base_url + '/deleteinstructor/' + instructorId
    }).then(res => {
      let newInstructor = instructors.filter(instructor => instructor._id !== instructorId);
      setInstructors(newInstructor);
    });
  };

  const handleInstructorNameChange = e => {
    setName(e.target.value);
  };

  const handleInstructorDescriptionChange = e => {
    setDescription(e.target.value);
  };

  const handleSaveInstructor = () => {
    axios({
      method: 'POST',
      url: base_url + '/addinstructor',
      data: {
        name: name,
        image: image,
        description: description
      }
    }).then(res => {
      let newInstructors = [...instructors];
      newInstructors.push(res.data);
      setInstructors(newInstructors);
      handleRefresh();
    });
  };

  return (
    <Fragment>
      <div className='d-flex'>
        <Sidebar />
        <div className='d-flex flex-column w-100'>
          <Topbar />
          <div className='content'>
            <h1 className='col-lg-10 text-primary mt-5 py-3 ml-5'>Instructors</h1>
            <div className='col-lg-10 justify-content-start mb-3 ml-5'>
              <Button color='primary' onClick={handleShowAddForm}>
                Add Instructor
              </Button>
              <InstructorAddForm
                showAddForm={showAddForm}
                setImage={setImage}
                handleShowAddForm={handleShowAddForm}
                handleInstructorNameChange={handleInstructorNameChange}
                handleInstructorDescriptionChange={handleInstructorDescriptionChange}
                name={name}
                image={image}
                description={description}
                handleSaveInstructor={handleSaveInstructor}
              />
              <table className='table table-striped border my-3'>
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Description</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {instructors.map(instructor => (
                    <InstructorRow
                      key={instructor._id}
                      instructor={instructor}
                      handleDeleteInstructor={handleDeleteInstructor}
                    />
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Instructors;
