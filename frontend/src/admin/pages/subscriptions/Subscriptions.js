import React, { Fragment, useEffect, useState } from 'react';
import { Sidebar, Topbar } from '../../layout/components';
import axios from 'axios';
import { SubscriptionRow } from './components';

const Subscriptions = () => {
   const base_url = process.env.REACT_APP_API_BASE_URL + '/admin';
  const [packagepayments, setPackagePayments] = useState([]);

  useEffect(() => {
    axios.get(base_url + '/showpackagepayments').then(res => {
      setPackagePayments(res.data);
    });
  }, []);

  return (
    <Fragment>
      <div className='d-flex'>
        <Sidebar />
        <div className='d-flex flex-column w-100'>
          <Topbar />
          <div className='content'>
            <h1 className='col-lg-10 text-primary mt-5 py-3 ml-5'>Subscriptions</h1>
            <div className='col-lg-10 justify-content-start mb-3 ml-5'>
              <table className='table table-striped border my-3'>
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Client Code</th>
                    <th>Customer Name</th>
                    <th>Packages</th>
                    <th>Classes</th>
                  </tr>
                </thead>
                <tbody>
                  {packagepayments.map(packagepayment => (
                    <SubscriptionRow
                      key={packagepayment._id}
                      packagepayment={packagepayment}
                    />
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Subscriptions;
