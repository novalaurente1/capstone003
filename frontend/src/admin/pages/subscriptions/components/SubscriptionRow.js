import React, { Fragment } from 'react';

const SubscriptionRow = props => {

  const packagepayment = props.packagepayment;

  const packagepaymentId = packagepayment._id;

  const packagepaymentCode = packagepaymentId.substr(17,7);

  const userId = packagepayment.userId;

  const customerCode = userId.substr(17,7);

  return(
    <Fragment>
      <tr>
        <td>{packagepaymentCode}</td>
        <td>{customerCode}</td>
        <td>{packagepayment.userName}</td>
        <td>{packagepayment.packageName}</td>
        <td>{packagepayment.packageSessions}</td>
      </tr>
    </Fragment>
  )
}

export default SubscriptionRow;