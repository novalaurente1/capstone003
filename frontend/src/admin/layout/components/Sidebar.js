import React, { Fragment, useState } from 'react';
import { Collapse } from 'reactstrap';

const Sidebar = () => {

  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <Fragment>

      <nav id='sidebar'>
        <div className='sidebar-header pt-1 px-0 border-bottom' style={{border: 'orange'}}>
          <a
            className='navbar-brand d-flex align-items-center py-0'
            href='/dashboard'>
            <img src="/ascend-yoga-logo.png" alt="person holding a sitting yoga pose" className="logo-img ml-3 mt-3" />
            <span className='logo-text-admin ml-3 mt-3'>Ascend Yoga</span>
          </a>
        </div>

        <ul className='list-unstyled components'>
          <li>
            <a href='#/admin/dashboard'>
              Dashboard
            </a>
          </li>

          <li>
            <a id="theLink" onClick={toggle}>
              Classes
              <i className="fa fa-caret-down" style={{marginLeft: 110}} aria-hidden="true"></i>
            </a>
            
            <Collapse isOpen={isOpen}>
              <ul className='list-unstyled'>
                <li>
                  <a href='#/admin/manageclasses'>
                    All Classes
                  </a>
                </li>
                <li>
                  <a href='#/admin/manageschedules'>
                    Class Schedules
                  </a>
                </li>
              </ul>
            </Collapse>
          </li>

          <li>
            <a href='#/admin/managesubscriptions'>
              Subscriptions
            </a>
          </li>
          <li>
            <a href='#/admin/managepackages'>
              Packages
            </a>
          </li>
          <li>
            <a href='#/admin/managecustomers'>
              Customers
            </a>
          </li>
          <li>
            <a href='#/admin/manageinstructors'>
              Instructors
            </a>
          </li>
        </ul>
      </nav>
    </Fragment>
  );
};

export default Sidebar;
