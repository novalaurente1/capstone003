import React, {useState, useEffect, Fragment} from 'react';
import { DisplayClassScheduleRow } from './components';
import axios from 'axios';
import { Navbar } from '../../layout';

const DisplayClassSchedules = () =>{

  const base_url = process.env.REACT_APP_API_BASE_URL + '/admin';
  const [classschedules, setClassSchedules] = useState([]);
  
  useEffect(() => {
    axios.get(base_url + '/showschedules').then(res=>{
      setClassSchedules(res.data)
    })
  }, []);

  return(
    <Fragment>
      <Navbar />
        <div>
          <h1 className="col-lg-10 offset-lg-1 text-center text-primary">Classes</h1>
          <div className="col-lg-10 offset-lg-1 justify-content-start mb-3">
            <table
              className="col-lg-10 offset-lg-1 table border my-3"
            >
              <thead>
                <tr className="table-active text-primary">
                  <th className="text-uppercase">Date</th>
                  <th className="text-uppercase">Time</th>
                  <th className="text-uppercase">Class</th>
                  <th className="text-uppercase">Instructor</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {classschedules.map(classschedule=>
                  <DisplayClassScheduleRow
                    key={classschedule._id}
                    classschedule={classschedule}
                  />
                )}
              </tbody>
            </table>
          </div>
        </div>
    </Fragment>
  )
}

export default DisplayClassSchedules;