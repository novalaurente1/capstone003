import React, { Fragment, useState } from 'react';
import { Button } from 'reactstrap';
import axios from 'axios';
import Swal from 'sweetalert2';

const DisplayClassScheduleRow = props => {

  const classschedule = props.classschedule;
  const base_url = process.env.REACT_APP_API_BASE_URL;
  
  const [attendance, setAttendance] = useState([]);
  
  const handleAddStudent = () => {
    
    const user = JSON.parse(sessionStorage.user);
    
    axios({
      method: 'POST',
      url: base_url + '/addstudent',
      data: {
      userId: user.id,
      userCustomerCode: user.customerCode,
      userName: user.name,
      classId: classschedule._id,
      className: classschedule.yogaclassName,
      classDate: classschedule.date,
      classStartTime: classschedule.startTime,
      instructorId: classschedule.instructorId,
      instructorName: classschedule.instructorName
      }
    }).then(res => {
      const responseData = res.data;

      if (responseData.error !== undefined) {
        // if there is an error in the response, show an error message.
        Swal.fire(
          'Booking failed!',
          responseData.error,
          'failed'
        );
      } else {
        // if there is no error in the response, show a success message.
        let newAttendance = [...attendance];
        newAttendance.push(res.data);
        setAttendance(newAttendance);
        Swal.fire(
          'Good job!',
          'You enrolled for a class!',
          'success'
          )
        window.location.replace('#/myaccount');
      }

    });

  }

  return(
    <Fragment>
      <tr className="bg-lightwarning">
        <td>{classschedule.date}</td>
        <td>{classschedule.startTime} - {classschedule.endTime}</td>
        <td>{classschedule.yogaclassName}</td>
        <td>{classschedule.instructorName}</td>
        <td>
          <Button
            color="primary"
            onClick={handleAddStudent}
          >
            Enroll Now
          </Button>
        </td>
      </tr>
    </Fragment>
  )
}

export default DisplayClassScheduleRow;