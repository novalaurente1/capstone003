import React, { Fragment, useEffect } from 'react';
import {Elements, StripeProvider} from 'react-stripe-elements';
import { CheckoutForm } from './components';
import { Link } from 'react-router-dom';

const Checkout = (props) => {
  if (props.location.state == undefined) {
    return <div>Cannot checkout an empty package</div>;
  } else {
    const {packageName, packagePrice, packageId} = props.location.state;

    return (
      <Fragment>
      <StripeProvider apiKey="pk_test_BmgdloH4vwCC6UenlXIBPfNG00PBW6IaTy">
        <div className="d-flex vh-100">
          <div className="w-50 bg-lightwarning d-flex align-items-center">
            <div className="col-lg-6 offset-lg-2">
              <h4>You will avail of</h4>
              <br />
              <h3>Package: <strong>{packageName}</strong></h3>
              <br />  
              <h2 className="text-primary">₱ {packagePrice}.00</h2>
              <br />
              <br />  
              <Link
                className="text-secondary" 
                to="/packages" 
              >
                <i className="fa fa-angle-double-left" aria-hidden="true">
                  <span style={{fontFamily: 'Ubuntu'}}> Return to Packages</span>
                </i>
              </Link>
            </div>
          </div>
          <div className="w-50 d-flex align-items-center">
            <Elements>
              <CheckoutForm 
                packageName={packageName}
                packageId={packageId}
                packagePrice={packagePrice}
              />
            </Elements>
          </div>
        </div>
        </StripeProvider>
      </Fragment>
    )
  }
}

export default Checkout;