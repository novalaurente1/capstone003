import React, { Fragment, useState } from 'react';
import { Label } from 'reactstrap';
import {FormInput} from '../../../../components';
import {CardElement, injectStripe} from 'react-stripe-elements';
import Swal from 'sweetalert2';


const CheckoutForm = (props) => {

  const [email, setEmail] = useState("");
  const [nameOnCard, setNameOnCard] = useState("");

  const submit = async (e) => {
    const tokenResponse = await props.stripe.createToken({name:"Name"})
    if(tokenResponse.token) {
      const user = JSON.parse(sessionStorage.user);

      const response = await fetch(process.env.REACT_APP_API_BASE_URL + '/charge', {
        method: "POST",
        headers: {"Content-type":"application/json"},
        body: JSON.stringify({
          tokenId: tokenResponse.token.id,
          userId: user.id,
          email: email,
          name: nameOnCard,
          description: props.packageName,
          packageId: props.packageId,
          amount: parseFloat(props.packagePrice)
        })
      });
  
      if (response.ok) {
        console.log("Purchase Complete");
        Swal.fire(
          'Good job!',
          'You availed a package!',
          'success'
        )
        window.location.replace("#/packages");
      };
    } else {
      window.alert('error generating a token');
    }

  }

  const handleEmailChange = (e) => {
    setEmail(e.target.value)
  }

  const handleNameOnCardChange = (e) => {
    setNameOnCard(e.target.value)
  }

  return (
    <Fragment>
          <div className="col-lg-8 offset-lg-2">
            <p className="text-center">Would you like to complete the purchase?</p>
            <FormInput 
              label={"Email"}
              type={"email"}
              name={"email"}
              onChange={handleEmailChange}
            />
            <Label
              className="mb-3"
            >Card Information</Label>
            <div className="border py-3 px-3">
              <CardElement />
            </div>
            <br />
            <FormInput 
              label={"Name on Card"}
              type={"text"}
              name={"nameOnCard"}
              onChange={handleNameOnCardChange}
            />

            <button
              onClick={submit}  
              className="btn btn-primary btn-block mt-3"
            >Pay ₱ {props.packagePrice}
            .00
            </button>
          </div>


    </Fragment>
  )
}

export default injectStripe(CheckoutForm);
