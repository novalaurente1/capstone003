import React, { Fragment } from 'react';
import { Navbar } from '../../layout';
import ReactDOM from 'react-dom';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { Carousel } from 'react-responsive-carousel';

const DisplayEvents = () => {
  return (
    <Fragment>
      <Navbar />
      <div>
        <h1 className='col-lg-10 offset-lg-1 text-center text-primary'>Events</h1>
        <div className='h-50 col-lg-3 mx-auto d-flex justify-content-center my-3'>
          <Carousel >
            <div>
              <img src='/poster1.jpg' />
            </div>
            <div>
              <img src='/poster2.jpg' />
            </div>
            <div>
              <img src='/poster3.jpg' />
            </div>
          </Carousel>
        </div>
      </div>
    </Fragment>
  );
};

export default DisplayEvents;
