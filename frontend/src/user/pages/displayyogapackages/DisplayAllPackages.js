import React, {useState, useEffect, Fragment} from 'react';
import { DisplayYogaPackagesRow } from './components';
import axios from 'axios';
import { Navbar } from '../../layout';

const DisplayYogaPackages = () =>{
  const base_url = process.env.REACT_APP_API_BASE_URL + '/admin';
  const [yogapackages, setYogaPackages] = useState([]);
  
  useEffect(() => {
    axios.get(base_url + '/showyogapackages').then(res=>{
      setYogaPackages(res.data)
    })
  }, []);

  return(
    <Fragment>
      <div>
        <h1 className="col-lg-10 offset-lg-1 text-center text-primary">All Packages</h1>
        <small className="text-center ml-2">Did not avail of any packages yet? Choose one below:</small>
        <div className="col-lg-10 justify-content-start mb-3">
          <table
            className="col-lg-6 table border my-3"
          >
            <thead>
              <tr className="table-active text-primary">
                <th className="text-center text-uppercase px-5">Package</th>
                <th className="text-center text-uppercase px-5">Price</th>
                <th className="text-center px-5"></th>
              </tr>
            </thead>
            <tbody>
              {yogapackages.map(yogapackage=>
                <DisplayYogaPackagesRow
                  key={yogapackage._id}
                  yogapackage={yogapackage}
                />
              )}
            </tbody>
          </table>
        </div>
      </div>
    </Fragment>
  )
}

export default DisplayYogaPackages;