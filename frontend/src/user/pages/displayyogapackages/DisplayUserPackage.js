import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

const DisplayUserPackage = props => {
  const { packageName, packageSessions, userSessions } = props.packagePayment;

  return (
    <Fragment>
      <div className='col-lg-10 d-flex flex-column align-items-center'>
        <div
          className='col-lg-6 py-3 my-3'
          style={{
            backgroundColor: '#FFF9EF',
            borderWidth: 1,
            borderColor: '#E2E2E2',
            borderStyle: 'solid'
          }}>
          <h3>
            Package: <span className='text-primary'>{packageName}</span>
          </h3>
          <h3>
            Number of Sessions: <span className='text-primary'>{userSessions}</span> out of{' '}
            {packageSessions} sessions left
          </h3>
        </div>
        <div className='col-lg-6 py-3 my-3 d-flex'>
          <Link className='text-primary mx-auto' to='/schedules'>
            View classes <i className="fa fa-angle-double-right" aria-hidden="true"></i>
          </Link>
        </div>
      </div>
    </Fragment>
  );
};

export default DisplayUserPackage;
