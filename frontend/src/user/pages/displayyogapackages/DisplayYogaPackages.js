import React, {useState, useEffect, Fragment} from 'react';
import axios from 'axios';
import { Navbar } from '../../layout';
import DisplayAllPackages from './DisplayAllPackages';
import DisplayUserPackage from './DisplayUserPackage';

const DisplayYogaPackages = () =>{  
  const [packagePayment, setPackagePayment] = useState(null);
  
  useEffect(() => {
    const base_url = process.env.REACT_APP_API_BASE_URL;
    const user = JSON.parse(sessionStorage.user);

    axios.get(base_url + '/userpackages/' + user.id).then(res=>{
      if (res.data.packagePayment !== null) {
        setPackagePayment(res.data.packagePayment);
      } else {
        setPackagePayment(null)
      }
    })
  }, []);

  return(
    <Fragment>
      <Navbar />
      <div className="d-flex align-items-center justify-content-center">
        {packagePayment !== null ? <DisplayUserPackage packagePayment={packagePayment} /> : <DisplayAllPackages />}
      </div>
    </Fragment>
  )
}

export default DisplayYogaPackages;