import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

const DisplayYogaPackagesRow = props => {

  const yogapackage = props.yogapackage;
    
  return(
    <Fragment>
      <tr className="bg-lightwarning">
        <td className="text-center px-5">{yogapackage.name}</td>
        <td className="text-center px-5">{yogapackage.price}</td>
        <td className="text-center px-5">          
          <Link 
          to={{ pathname: '/checkout', state: { packageId: yogapackage._id, packageName : yogapackage.name, packagePrice: yogapackage.price }}}
          className="btn btn-primary px-5"
          >
            Avail
          </Link>
        </td>
      </tr>
    </Fragment>
  )
}

export default DisplayYogaPackagesRow;