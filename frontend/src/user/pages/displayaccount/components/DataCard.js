import React, { Fragment, useState, useEffect } from 'react';
import { Card, CardText, CardBody } from 'reactstrap';
import axios from 'axios';

const DataCard = props => {

  const userClass = props.userClass

  return (
    <Fragment>
      <div className='col-lg-4 stretch-card grid-margin my-1'>
        <Card style={{ backgroundColor: '#fff9ef' }}>
          <CardBody className='mr-auto'>
            <CardText>You've booked a class on&nbsp; 
            <span className="text-primary">{userClass.className}</span> by&nbsp; 
            <span className="text-primary">{userClass.instructorName}</span> on&nbsp; 
            <span className="text-primary">{userClass.classDate}</span> at&nbsp; 
            <span className="text-primary">{userClass.classStartTime}</span>
            </CardText>
          </CardBody>
        </Card>
      </div>
    </Fragment>
  );
};

export default DataCard;
