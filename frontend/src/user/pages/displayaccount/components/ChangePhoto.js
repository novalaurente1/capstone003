import React from 'react';
import { Modal, ModalHeader, ModalBody, Button } from 'reactstrap';
import Image from './Image';

const ChangePhoto = props => {

  return (
    <Modal isOpen={props.showChangePhotoForm} toggle={props.showChangePhoto}>
      <ModalHeader toggle={props.showChangePhoto}>Change Photo</ModalHeader>
      <ModalBody>
        <label htmlFor="image">Select from file</label>
        <Image setImage={props.setImage} />
        <div className='d-flex justify-content-end'>
          <Button
            color='primary'
            onClick={props.handleSavePhoto}
            Change Photo
          >
            Save Photo
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};

export default ChangePhoto;
