import DataCard from './DataCard';
import ChangePhoto from './ChangePhoto';

export {
  DataCard,
  ChangePhoto
}