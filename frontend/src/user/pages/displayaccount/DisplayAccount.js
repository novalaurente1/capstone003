import React, { Fragment, useState, useEffect } from 'react';
import { DataCard, ChangePhoto } from './components';
import { Navbar } from '../../layout';
import { Card, CardText, CardBody } from 'reactstrap';
import axios from 'axios';
import Swal from 'sweetalert2';

const DisplayAccount = () => {
  const base_url = process.env.REACT_APP_API_BASE_URL;
  const [user, setUser] = useState({});
  const [customer, setCustomer] = useState({});
  const [userClasses, setUserClasses] = useState([]);
  const [showChangePhotoForm, setShowChangePhotoForm] = useState(false);
  const [profilePicture, setImage] = useState('');

  useEffect(() => {
    if (sessionStorage.token) {
      let user = JSON.parse(sessionStorage.user);
      setUser(user);
    } else {
      window.location.replace('#/login');
    }
  }, []);

  useEffect(() => {
    if(user.id !== undefined) {
      getCustomerData();
      getCustomerClasses();
    }
  }, [user.id]);

  const getCustomerClasses = () => {
    axios.get(base_url + '/admin/showclassesbyuserid/' + user.id).then(res => {
      setUserClasses(res.data);
    });
  };

  const getCustomerData = () => {
    axios.get(base_url + '/admin/showuserbyid/' + user.id).then(res => {
      setCustomer(res.data);
    });
  };

  const customerId = customer._id;

  if (customerId != null) {
    var customerCode = customerId.substr(17, 7);
  }

  const showChangePhoto = () => {
    setShowChangePhotoForm(!showChangePhotoForm);
  };

  const handleSavePhoto = () => {
    axios({
      method: 'PATCH',
      url: base_url + '/updateprofilepicture/' + user.id,
      data: {
        profilePicture: profilePicture
      }
    }).then(res => {
      setImage(res.data);
      Swal.fire(
        'Good job!',
        'You updated your profile picture!',
        'success'
      )
      showChangePhoto();
      getCustomerData();
      window.location.replace('#/myaccount');
    });
  }

  return (
    <Fragment>
      <Navbar />
      <div>
        <h1 className='col-lg-10 offset-lg-1 text-center text-primary'>My Account</h1>
        <div className='col-lg-10 offset-lg-1 d-flex flex-column mb-3'>
          <div className='d-flex justify-content-center my-3'>
            <div className='d-flex flex-column align-items-center'>
              <img
                className='mt-1 mx-2'
                src={customer.profilePicture === undefined
                ? '/portrait_placeholder.png'
                :  base_url + "/images/uploads/" + customer.profilePicture}
                
                style={{ borderRadius: 50 }}
                height='100px'
                width='100px'
                alt='portrait placeholder'
              />
              <button 
                className='btn btn-link'
                onClick={showChangePhoto}
              >Change Photo</button>
              <ChangePhoto 
                showChangePhotoForm={showChangePhotoForm}
                showChangePhoto={showChangePhoto}
                handleSavePhoto={handleSavePhoto}
                setImage={setImage}
                profilePicture={profilePicture}
              />
            </div>

            <div className='pl-0 col-lg-3 stretch-card grid-margin'>
              <Card style={{ backgroundColor: '#fff9ef' }}>
                <CardBody className='mr-auto'>
                  <CardText>
                    Name: <span className='text-primary'>{user.name}</span>
                  </CardText>
                  <CardText>
                    ID: <span className='text-primary'>{customerCode}</span>
                  </CardText>
                  <CardText>
                    Member since: <span className='text-primary'>{customer.dateRegistered}</span>
                  </CardText>
                </CardBody>
              </Card>
            </div>
          </div>
          <div className='d-flex flex-column align-items-center justify-content-center'>
            {userClasses.map(userClass => 
              <DataCard 
                key={userClass._id} 
                userClass={userClass}
              />
            )}
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default DisplayAccount;
