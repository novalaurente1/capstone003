import React, { useState, useEffect } from 'react';
import { Navbar, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import axios from 'axios';

const Navibar = props => {
  const [user, setUser] = useState({});

  useEffect(() => {
    if (sessionStorage.token) {
      let user = JSON.parse(sessionStorage.user);
      setUser(user);
    } else {
      window.location.replace('#/login');
    }
  }, []);

  const handleLogout = () => {
    sessionStorage.clear();
    window.location.replace('#/');
  };

  return (
    <div>
      <Navbar color='primary' light>
        <NavbarBrand href='/' className='text-white'>
          <img
            src='/ascend-yoga-logo-white.png'
            alt='person holding a sitting yoga pose'
            className='logo-img ml-3'
          />
          <span className='ml-3' style={{ fontWeight: 600 }}>
            Ascend Yoga
          </span>
        </NavbarBrand>
        <Nav className='mr-auto'>
          <NavItem>
            <NavLink className='text-white text-uppercase' href='#/packages'>
              Package
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className='text-white text-uppercase' href='#/myaccount'>
              Account
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className='text-white text-uppercase' href='#/schedules'>
              Classes
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className='text-white text-uppercase' href='#/events'>
              Events
            </NavLink>
          </NavItem>
        </Nav>
        <Nav className='ml-auto'>
          <NavItem>
            <NavLink className='text-white text-uppercase' href='#'>
              Hello, {user.name}
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className='text-white text-uppercase' onClick={handleLogout} style={{cursor: 'pointer'}}>
              Logout
            </NavLink>
          </NavItem>
        </Nav>
      </Navbar>
    </div>
  );
};

export default Navibar;
